package utilities

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.ints.shouldBeExactly
import io.kotest.matchers.shouldBe
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.ReferenceRange

class RangeUtilsTest : StringSpec({
    "Test Range Parsing" {
        val rangeString = "1:100-200,1:400-700,2:500-800,1:800-900"
        val rangesToCompare = listOf<ReferenceRange>(ReferenceRange("userDefinedRange", Chromosome.instance(1),100,200,0),
            ReferenceRange("userDefinedRange", Chromosome.instance(1),400,700,0),
            ReferenceRange("userDefinedRange", Chromosome.instance(1),800,900,0),
            ReferenceRange("userDefinedRange", Chromosome.instance(2),500,800,0)
            )
        val ranges = parseRangeString(rangeString)
        ranges.size shouldBeExactly 4

        //This also tests the sort as well
        for(i in ranges.indices) {
            val currentParsedRange = ranges[i]
            val currentTestRange = rangesToCompare[i]
            currentParsedRange.chromosome() shouldBe currentTestRange.chromosome()
            currentParsedRange.start() shouldBe currentTestRange.start()
            currentParsedRange.end() shouldBe currentTestRange.end()
        }
    }
    "Test Range Overlaps" {
        val rangeString = "1:100-200,1:400-700,2:500-800,1:800-900"
        val ranges = parseRangeString(rangeString)

        val rangeString2 = "1:1-50,1:199-300,1:825-875"
        val ranges2 = parseRangeString(rangeString2)

        val filteredRanges = filterGraphByRequestedRanges(ranges, ranges2)

        filteredRanges.size shouldBeExactly 2

        val rangesToCompare = listOf<ReferenceRange>(ReferenceRange("userDefinedRange", Chromosome.instance(1),100,200,0),
            ReferenceRange("userDefinedRange", Chromosome.instance(1),800,900,0),
        )
        for(i in filteredRanges.indices) {
            val currentParsedRange = filteredRanges[i]
            val currentTestRange = rangesToCompare[i]
            currentParsedRange.chromosome() shouldBe currentTestRange.chromosome()
            currentParsedRange.start() shouldBe currentTestRange.start()
            currentParsedRange.end() shouldBe currentTestRange.end()
        }

    }
    "Test Range Overlaps Across Chroms" {
        val rangeString = "1:100-200,1:400-700,2:500-800,1:800-900,3:450-600,5:500-700,10:400-700"
        val ranges = parseRangeString(rangeString)

        val rangeString2 = "1:1-50,1:199-300,1:825-875,1:850-900,10:500-600,3:400-449,5:400-600"
        val ranges2 = parseRangeString(rangeString2)


        val filteredRanges = filterGraphByRequestedRanges(ranges, ranges2)

        filteredRanges.size shouldBeExactly 4

        val rangesToCompare = listOf<ReferenceRange>(ReferenceRange("userDefinedRange", Chromosome.instance(1),100,200,0),
            ReferenceRange("userDefinedRange", Chromosome.instance(1),800,900,0),
            ReferenceRange("userDefinedRange", Chromosome.instance(5),500,700,0),
            ReferenceRange("userDefinedRange", Chromosome.instance(10),400,700,0)
        )


        for(i in filteredRanges.indices) {
            val currentParsedRange = filteredRanges[i]
            val currentTestRange = rangesToCompare[i]
            currentParsedRange.chromosome() shouldBe currentTestRange.chromosome()
            currentParsedRange.start() shouldBe currentTestRange.start()
            currentParsedRange.end() shouldBe currentTestRange.end()
        }
    }

    "Test Full Chrom Overlap" {
        val rangeString = "1:100-200,1:400-700,2:500-800,1:800-900,3:450-600,5:500-700,10:400-700"
        val ranges = parseRangeString(rangeString)

        val rangeString2 = "1,3:400-449,6"
        val ranges2 = parseRangeString(rangeString2)

        val filteredRanges = filterGraphByRequestedRanges(ranges, ranges2)

        filteredRanges.size shouldBeExactly 3

        val rangesToCompare = listOf<ReferenceRange>(ReferenceRange("userDefinedRange", Chromosome.instance(1),100,200,0),
            ReferenceRange("userDefinedRange", Chromosome.instance(1),400,700,0),
            ReferenceRange("userDefinedRange", Chromosome.instance(1),800,900,0),
        )


        for(i in filteredRanges.indices) {
            val currentParsedRange = filteredRanges[i]
            val currentTestRange = rangesToCompare[i]
            currentParsedRange.chromosome() shouldBe currentTestRange.chromosome()
            currentParsedRange.start() shouldBe currentTestRange.start()
            currentParsedRange.end() shouldBe currentTestRange.end()
        }
    }

})
