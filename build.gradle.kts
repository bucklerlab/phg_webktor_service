import org.gradle.api.JavaVersion.VERSION_11
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val kotlinVersion = "1.6.21"

val ktorVersion = "2.2.4"
val exposedVersion = "0.26.2"
val h2Version = "1.4.200"
val hikariCpVersion = "3.4.5"
val logbackVersion = "1.2.3"
val assertjVersion = "3.16.1"
val restAssuredVersion = "4.3.1"
val junitVersion = "5.6.2"

plugins {
    kotlin("jvm") version "1.6.21"
    kotlin("plugin.serialization") version "1.6.21"
    java
    application
    id("com.github.johnrengelman.shadow") version "6.1.0"
}

repositories {
    jcenter()
    mavenCentral()
    maven("https://repo1.maven.org/maven2")
    maven("http://maven.imagej.net/content/groups/public/") { this.isAllowInsecureProtocol = true }
}

group = "net.maizegenetics"
version = "1.0-SNAPSHOT"

dependencies {

    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-server-content-negotiation:$ktorVersion")
    implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
    implementation("io.ktor:ktor-server-websockets:$ktorVersion")
    implementation("io.ktor:ktor-server-default-headers:$ktorVersion")
    implementation("io.ktor:ktor-server-call-logging:$ktorVersion")
    implementation("io.ktor:ktor-serialization-jackson:$ktorVersion")

    implementation("org.graalvm.sdk:graal-sdk:20.0.0")

    implementation("org.postgresql:postgresql:42.2.10")
    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("com.zaxxer:HikariCP:$hikariCpVersion")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinVersion")
    implementation("org.openapitools:openapi-generator-gradle-plugin:3.3.4")
    implementation("net.maizegenetics:phg:1.4")

    implementation("io.kotest:kotest-runner-junit5-jvm:4.1.1")

    implementation("khttp:khttp:1.0.0")

    implementation("com.github.jengelman.gradle.plugins:shadow:6.1.0")

    implementation("org.ehcache:ehcache:3.9.5")

    val kotestVersion = "4.2.3"
    listOf("runner-junit5", "assertions-core", "property").forEach {
        testImplementation("io.kotest:kotest-$it-jvm:$kotestVersion")
    }
    testImplementation("org.assertj:assertj-core:$assertjVersion")
    testImplementation("io.rest-assured:rest-assured:$restAssuredVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

application {
    mainClassName = "MainKt"
}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions.jvmTarget = "11"
}

tasks.withType<Test> {
    useJUnitPlatform()
}
configurations.all {
    exclude("org.apache.logging.log4j")
}
