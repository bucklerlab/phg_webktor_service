package api

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import model.EdgeListResponse
import model.EdgeListResponseResult
import model.Metadata
import service.EdgeService

fun Route.edges() {
    val edgeService = EdgeService()
    route("/edges") {

        get("") {
            val edges = edgeService.getAllEdges()

            if(edges.isEmpty()) {
                call.respond(" ${HttpStatusCode.NotFound}: No Edges found in the DB.")
            }
            else {
                call.respond(EdgeListResponse(Metadata(), EdgeListResponseResult(edges.toTypedArray())))
            }
        }

    }
}