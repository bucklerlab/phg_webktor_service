package utilities

import service.DataSource

/**
 * Function to return method names from a PHG db methods table.
 * input:  a list of types.  If this list is null, all method names are returned.
 *          If the list is non-null, only method names for entries with the specified
 *          method_type are returned.
 */
fun getPHGMethodNamesForMethodType(types:List<Int>?): List<String> {

    val sqlSB = StringBuilder()
    sqlSB.append("SELECT name from methods")
    if (types != null) {
        val typeString = types.joinToString(",")
        sqlSB.append(" WHERE method_type IN ( ${typeString})")
    }

    sqlSB.append(";")
    val sqlStatement = sqlSB.toString()

    println("getPHGMethodNamesForMethodType: query statement: ${sqlStatement}")

    // The connection is automatically closed when this block finishes.
    DataSource.connection.use { connection ->

        try {

            val result = mutableListOf<String>()
            connection.createStatement().executeQuery(sqlStatement).use { rs ->

                val time = System.nanoTime()
                while (rs.next()) {
                    val name = rs.getString("name")
                    result.add(name)
                }

                println("getPHGMethodNamesForMethodType: time: ${(System.nanoTime() - time) / 1e9} secs.")
                return result

            }
        } catch (exc: Exception) {
            exc.printStackTrace()
            throw IllegalStateException("getPHGMethodNamesForMethodType: Problem querying the database: ${exc.message}")
        }

    }
}