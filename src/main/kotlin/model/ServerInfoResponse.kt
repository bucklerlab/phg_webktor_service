package model

import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class ServerInfoResponse(val metadata: Metadata,
                              val result: ServerInfo,
                              val _atContext: Context? = null) {

    override fun equals(o: Any?): Boolean {
        if (this === o) {
            return true
        }
        if (o == null || o !is ServerInfoResponse) {
            return false
        }
        val serverInfoResponse = o as ServerInfoResponse
        return this._atContext == serverInfoResponse._atContext &&
                metadata == serverInfoResponse.metadata &&
                result == serverInfoResponse.result
    }

    override fun hashCode(): Int {
        return Objects.hash(_atContext, metadata, result)
    }

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("class ServerInfoResponse {\n")
        sb.append("    _atContext: ").append(toIndentedString(_atContext)).append("\n")
        sb.append("    metadata: ").append(toIndentedString(metadata)).append("\n")
        sb.append("    result: ").append(toIndentedString(result)).append("\n")
        sb.append("}")
        return sb.toString()
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private fun toIndentedString(o: Any?): String? {
        return o?.toString()?.replace("\n", "\n    ") ?: "null"
    }


}