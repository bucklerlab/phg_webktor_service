package service

import model.Edge
import net.maizegenetics.pangenome.api.HaplotypeEdge
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import org.apache.log4j.Logger

class EdgeService {

    private val myLogger = Logger.getLogger(EdgeService::class.java)

    fun getAllEdges() : List<Edge> {
        val methodIds = getMethodIdsFromDB()

        return methodIds.asSequence() //Casting to sequence so its lazy
                .map { buildGraph(it) }
                .filterNotNull()
                .flatMap { getAllEdgesFromGraph(it) }
                .toList()
    }

    fun getAllEdgesFromGraph(graph:HaplotypeGraph) : List<Edge> {

        val existingEdges = graph.referenceRanges()
                .flatMap { graph.nodes(it) }
                .flatMap { graph.rightEdges(it)}
                .map { Pair(Pair(it.leftHapNode(), it.rightHapNode()), it) }
                .toMap()

        return graph.referenceRangeList().sorted()
                .zipWithNext()
                .map { Pair(graph.nodes(it.first), graph.nodes(it.second)) }
                .flatMap { buildEdges(it, existingEdges) }
    }

    fun buildEdges(leftAndRightNodes : Pair<List<HaplotypeNode>, List<HaplotypeNode>>, knownEdges : Map<Pair<HaplotypeNode, HaplotypeNode>,HaplotypeEdge>) : List<Edge>{
        val leftNodes = leftAndRightNodes.first
        val rightNodes = leftAndRightNodes.second

        return leftNodes.flatMap { leftNode ->
            val currentEdgeWeights = rightNodes.map { rightNode ->
                if(knownEdges.containsKey(Pair(leftNode, rightNode))) {
                    Pair(rightNode, knownEdges[Pair(leftNode,rightNode)]?.edgeProbability())
                }
                else {
                    Pair(rightNode,0.001)
                }
            }
            .toMap()

            val totalWeight = currentEdgeWeights.map { it.value ?: 0.0 }.sum()

            currentEdgeWeights.map { (rightNode, weight) ->
                val scaledWeight = (weight?:0.0)/totalWeight
                Edge("${leftNode.id()}_${rightNode.id()}", leftNode.id(), rightNode.id(), scaledWeight)
            }
        }
    }

    fun getEdgesFromGraph(graph: HaplotypeGraph) : List<Edge> {
        return graph.referenceRanges()
                .flatMap { graph.nodes(it) }
                .flatMap { graph.rightEdges(it) }
                .map { createAPIEdge(it) }
    }

    fun createAPIEdge(haplotypeEdge : HaplotypeEdge) : Edge {
        val leftId = haplotypeEdge.leftHapNode().id()
        val rightId = haplotypeEdge.rightHapNode().id()

        val weight = haplotypeEdge.edgeProbability()

        return Edge("${leftId}_${rightId}", leftId, rightId, weight)
    }

    fun getSingleEdge(id : String) : Edge {
        val idPair = parseEdgeId(id)

        val leftId = idPair.first
        val rightId = idPair.second


        //Determine which graph these two nodes are in by method

        //Get the graph

        //get the edgeWeight

        return Edge("${leftId}_${rightId}", leftId, rightId, 0.0)
    }

    fun parseEdgeId(id : String) : Pair<Int, Int> {
        val idSplit = id.split("_")

        check(idSplit.size == 2) {"Error in EdgeService.parseEdgeId.  edgeDbId must have 2 integer ids separated by an underscore."}

        return Pair(idSplit[0].toInt(), idSplit[1].toInt())
    }

}