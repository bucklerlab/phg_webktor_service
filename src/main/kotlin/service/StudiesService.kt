package service

import com.typesafe.config.ConfigFactory
import io.ktor.server.config.*
import io.ktor.util.*
import model.Study
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import org.apache.log4j.Logger

/**
 * For PHG, a Studies are methods.  The PHG method table provides method_id, method type, name and
 * description.
 *
 * We only fill in these fields, values from the PHG methods table:
 *   studyDbId = method_id
 *   studyDescription = description
 *   studyName = name
 *   studyType - this is enum, but values not stored in db.  Is it reasonable to copy enum here??
 */
class StudiesService {
    private val myLogger = Logger.getLogger(StudiesService::class.java)

    private val config = HoconApplicationConfig(ConfigFactory.load())

    @KtorExperimentalAPI
    val phgConfigFile = config.property("phgConfigFile").getString()

    /**
     * get method data for a specific PHG method id
     */
    fun getMethodDataForId(id:Int): Study? {

        val sqlStatement = "SELECT  method_id, method_type, name, description " +
                "FROM methods WHERE method_id=${id} ORDER BY method_id;"

        myLogger.info("allMethodNames: query statement: $sqlStatement")

        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->

            try {

                connection.createStatement().executeQuery(sqlStatement).use { rs ->

                    val time = System.nanoTime()

                    if (rs.next()) {
                        // should only be 1
                        val methodInt = rs.getInt("method_type")
                        var methodName:String? = null
                        for (current in DBLoadingUtils.MethodType.values()) {
                            if (current.value == methodInt) {
                                // find the name, e.g. "PATHS" or "ASSEMBLY_HAPLOTYPES"
                                methodName = current.toString()
                            }
                        }
                        return  Study(studyDbId = id.toString(), studyDescription = rs.getString("description"), studyName = rs.getString("name"), studyType = methodName)
                    } else {
                        myLogger.warn("getMethodDataForId: no study/methods data for id ${id}")
                        return null
                    }

                    myLogger.info("getMethodDataForId: processed in ${(System.nanoTime() - time) / 1e9} secs.")
                }
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                exc.printStackTrace()
                throw IllegalStateException("StudiesService: getMethodData: Problem querying the database: ${exc.message}")
            }
        }

    }

    /**
     * This gets all data from the PHG methods table, translates to fields in the Study model
     */
    fun allMethodNames(): List<Study> {

        val sqlStatement = "SELECT  method_id, method_type, name, description " +
                "FROM methods " +
                "ORDER BY method_id;"

        myLogger.info("allMethodNames: query statement: $sqlStatement")

        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->

            try {

                val result = arrayListOf<Study>()
                connection.createStatement().executeQuery(sqlStatement).use { rs ->

                    val time = System.nanoTime()

                    while (rs.next()) {
                        val methodInt = rs.getInt("method_type")
                        var methodName:String? = null
                        for (current in DBLoadingUtils.MethodType.values()) {
                            if (current.value == methodInt) {
                                // find the name, e.g. "PATHS" or "ASSEMBLY_HAPLOTYPES"
                                methodName = current.toString()
                            }
                        }
                        result.add(Study(studyDbId = rs.getInt("method_id").toString(), studyDescription = rs.getString("description"), studyName = rs.getString("name"), studyType = methodName))
                    }

                    myLogger.info("allMethodNames: number of methods: ${result.size} processed in ${(System.nanoTime() - time) / 1e9} secs.")
                    return result
                }
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                exc.printStackTrace()
                throw IllegalStateException("StudiesService: allMethodNames: Problem querying the database: ${exc.message}")
            }
        }
    }
}