package api

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import model.CallSetsListResponse
import model.CallSetsListResponseResult
import model.Metadata
import service.CallSetsService

/**
 * This file extends Ktor Route for BrAPI CallSets. It accepts REST "GET" requests.
 * TODO - Add support for additional REST requests (POST)
 *
 * @author  lcj34
 */
fun Route.callSets() {

    val callSetsService = CallSetsService()
    route("/callsets") {

        get("") {
            call.respond(
                CallSetsListResponse(
                    Metadata(),
                    CallSetsListResponseResult(callSetsService.generateCallSetsFromGraph().toTypedArray())
                )
            )
        }

        get("/{callSetDbId}") {
            val id = call.parameters["callSetDbId"]?.toInt() ?: throw IllegalStateException("Must provide callSetDbId")
            // TODO: what should be here?

        }
    }
}