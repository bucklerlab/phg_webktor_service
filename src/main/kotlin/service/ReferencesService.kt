package service

import model.Reference
import model.ReferenceSet
import org.apache.log4j.Logger
import java.util.stream.Collectors

class ReferencesService {
    private val myLogger = Logger.getLogger(ReferencesService::class.java)

    /**
     * This gets data from the PHG genome_file_data and reference_ranges tables,
     * translates to fields in the References model
     *
     * We need
     *   referenceName = chromosome name
     *   referenceSetDbID - genome_file_data table id for the reference
     *   sourceURI - genome_file_data table genome_path/genome_file for reference
     */
    fun getReferences(): List<Reference> {

        val time = System.nanoTime()
        val chromList = getChromNames()

        // for reference data with no id, the id will always be 1.
        // in the genome_file_data table, reference data is always added as the first entry
        // when the reference is added.

        // Get the referenceSetDBID, and sourceURI for the reference, then
        // create a list of Reference, one for each chromosome from the chrom list,
        // "chrom" becomes the referenceName, the other parameters are same for each object on the list
        val result = getReferenceData()

        val resultSet = chromList.map{
            chrom -> Reference(referenceName=chrom, referenceSetDbId=result.referenceSetDbId, sourceURI=result.sourceURI)
        }
        myLogger.info("getReferences:  processed in ${(System.nanoTime() - time) / 1e9} secs.")
        return resultSet
    }

    /**
     * Gets reference object for a specific chromosome.
     * This  first verifies the specified chromosome is in the db
     * If no matching chrom in the db, null is returned.
     */
    fun getReferencesForId(chrom:String): Reference? {

        val time = System.nanoTime()

        val chromList = getChromNames()
        if (!chromList.contains(chrom)) return null

        val result = getReferenceData()
        val refForId = Reference(referenceName=chrom, referenceSetDbId=result.referenceSetDbId, sourceURI=result.sourceURI)

        return refForId
    }

    // Get DB data and return select fields in the Reference object
    fun getReferenceData() : Reference {
        val sqlStatement = "SELECT  gt.line_name, gfd.id, gfd.genome_path, gfd.genome_file, gfd.file_checksum " +
                "FROM genotypes  gt, genome_file_data gfd " +
                "WHERE gt.genoid=gfd.genoid AND gt.genoid=1";

        myLogger.info("getReferenceGenomeData: query genome data statement: ${sqlStatement}")
        var result = Reference()
        DataSource.connection.use { connection ->
            try {
                connection.createStatement().executeQuery(sqlStatement).use { rs ->
                    if (rs.next()) {
                        val id = rs.getInt("id")
                        val sourceURI = "${rs.getString("genome_path")}/${rs.getString("genome_file")}"
                        result = Reference(  referenceSetDbId = rs.getInt("id").toString(), sourceURI=sourceURI)
                    }
                }
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                exc.printStackTrace()
                throw IllegalStateException("ReferenceSetService: getReferenceGenomeData: Problem querying the database: ${exc.message}")
            }
        }
        return result
    }

}