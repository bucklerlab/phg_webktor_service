package model

import kotlinx.serialization.Serializable

/**
 *
 * @param Atcontext
 * @param metadata
 * @param result
 */
@Serializable
data class CallsListResponse(
        val metadata: MetadataTokenPagination,
        val result: CallsListResponseResult,
        var Atcontext: Context? = null
)

@Serializable
data class CallsListResponseResult (val data: Array<Call> )