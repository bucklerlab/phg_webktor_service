package model

import kotlinx.serialization.Serializable

@Serializable
data class VariantTableResponse(
    val metadata: MetadataTokenPagination,
    var result: VariantTable,
    val atcontext: Context? = null
)

data class VariantTableListResponse(
    val metadata: MetadataTokenPagination,
    val result: VariantTableListResponseResult,
    var Atcontext: Context? = null
)

data class VariantTableListResponseResult(val data: Array<VariantTable>)