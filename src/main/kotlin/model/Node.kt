package model

import java.util.*

class Node(val nodeDbId : Int, val additionalInfo : Map<String,Any> = mapOf()) {

    override fun equals(o: Any?): Boolean {
        if (this === o) {
            return true
        }
        if (o == null || o !is Node) {
            return false
        }
        val node: Node = o as Node
        return nodeDbId == node.nodeDbId &&
                additionalInfo == node.additionalInfo
    }

    override fun hashCode(): Int {
        return Objects.hash(nodeDbId, additionalInfo)
    }

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("class Node {\n")
        sb.append("    nodeDbId: ").append(toIndentedString(nodeDbId)).append("\n")
        sb.append("    additionalInfo: ").append(toIndentedString(additionalInfo)).append("\n") //This one might need work
        sb.append("}")
        return sb.toString()
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private fun toIndentedString(o: Any?): String? {
        return o?.toString()?.replace("\n", "\n    ") ?: "null"
    }
}