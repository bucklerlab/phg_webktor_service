package utilities

import model.Variant
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.ReferenceRange

/**
 * Function to filter the reference ranges from the graph using the rangesToKeep list of reference ranges coming in from the user request.
 *
 * This will efficiently determine which reference ranges are overlapping the specified ranges.
 */
fun filterGraphByRequestedRanges(referenceRangeList: List<ReferenceRange>, rangesToKeep: List<ReferenceRange>): List<ReferenceRange> {
    if(rangesToKeep.isEmpty()) return referenceRangeList

    //Sort both sets so we can linearly check them
    val sortedGraphRanges = referenceRangeList.sorted()
    val sortedUserRanges = rangesToKeep.sorted()


    var graphIndex = 0
    var userIndex = 0

    val outputRangeSet = mutableSetOf<ReferenceRange>()

    //Can fail out once one of the two iterators goes past the list as there will be nothing else to add
    while(graphIndex < sortedGraphRanges.size && userIndex < sortedUserRanges.size) {
        val currentGraphRange = sortedGraphRanges[graphIndex]
        val currentUserRange = sortedUserRanges[userIndex]

        //check if they are overlapping
        if(rangesOverlap(currentGraphRange, currentUserRange)) {
            //add currentGraphRange to the set
            outputRangeSet.add(currentGraphRange)

            //We then want to slide the graph index up one and allow the loop to continue
            graphIndex++
        }
        else if(isUserRangeBeforeGraph(currentGraphRange, currentUserRange)) {
            //if user range is before the graph, we just need to slide up the userIndex and allow loop to continue
            userIndex++
        }
        else { //Means the graphRange is before the current user Range
            //Need to slide up the graph to the next one and compare against the user range again
            graphIndex++
        }
    }

    return outputRangeSet.toList().sorted()
}

/**
 * Simple function to check to see if 2 ranges will overlap
 *
 * If the chroms do not match, they automatically do not overlap
 *
 * If the user specified range has -1 for start and end it is a special case for the full chromosome
 *
 * Otherwise check boundaries using the logic provided here:
 * https://stackoverflow.com/questions/3269434/whats-the-most-efficient-way-to-test-two-integer-ranges-for-overlap
 */
private fun rangesOverlap(currentGraphRange : ReferenceRange, currentUserRange : ReferenceRange) : Boolean {
    return when {
        (currentGraphRange.chromosome() != currentUserRange.chromosome()) -> false //Chroms do not match, cannot overlap
        (currentUserRange.start()==-1 && currentUserRange.end() == -1) -> true //User range is asking for full chrom, we already know they match as it has fallen through
        else -> (currentGraphRange.start() <= currentUserRange.end() && currentUserRange.start() <= currentGraphRange.end()) //Check actual boundaries
    }
}

/**
 * Simple function to check to see if the user requested range is before the current graph range.
 *
 * If this is true, the code which walks over the two range lists will need to update the current user range as it is 'behind' the graph.
 */
private fun isUserRangeBeforeGraph(currentGraphRange: ReferenceRange, currentUserRange: ReferenceRange): Boolean {
    return when {
        (currentGraphRange.chromosome() > currentUserRange.chromosome()) -> true //If the graph chrom is after the user chrom, user chrom is before graph
        (currentGraphRange.chromosome() < currentUserRange.chromosome()) -> false //If the user chrom is after graph chrom, user chrom is after graph
        else -> (currentUserRange.end() < currentGraphRange.start()) //Else chroms match so check the start and end.
    }
}


/**
 * Function to parse the range string coming in over the URL into a list of Range objects
 *
 * Ranges will be separated by a ',' and will take the form Chrom:Start-End where Start and End are 1 based inclusive inclusive
 *
 * If no start or end are specified, it will return the full chromosome.
 */
fun parseRangeString(rangeString: String) : List<ReferenceRange> {
    return rangeString.split(",")
        .filter { it.isNotEmpty() }
        .map { parseSingleRangeString(it) }
        .sorted()
}

/**
 * Function to parse a single range interval string.
 *
 * The range will take the form Chrom:Start-End where Start and End are 1 based inclusive inclusive
 *
 * If no start or end are specified, it will return the full chromosome.
 *
 * It will return a ReferenceRange object holding the range with dummy values for the other fields.
 */
fun parseSingleRangeString(singleRangeString : String) : ReferenceRange {
    val rangeSplitOnColon = singleRangeString.split(":")
    if(rangeSplitOnColon.size==1) {
        //Means we need to cover the whole chromosome
        return ReferenceRange("userDefinedRange", Chromosome.instance(rangeSplitOnColon.first().trim()), -1,-1,0)
    }
    val chrom = Chromosome.instance(rangeSplitOnColon.first().trim())

    val stAndEnd = rangeSplitOnColon.last().trim().split("-")
    check(stAndEnd.size==2) {"Incorrect number of positions"}
    return ReferenceRange("userDefinedRange",chrom,stAndEnd.first().trim().toInt(), stAndEnd.last().trim().toInt(),0)
}

/**
 * Function to build a Variant record for a single reference range.  VariantSetDbId needs to be passed in otherwise we will not be able to add in the id.
 */
fun convertReferenceRangeToVariant(refRange: ReferenceRange, variantSetDbId: String, graph: HaplotypeGraph?=null) : Variant {
    val altAlleles = if(graph==null) { listOf() }
                    else { buildAltAlleleList(refRange,graph) }

    return Variant(end = refRange.end(), referenceBases = "", referenceName = refRange.chromosome().name, start = refRange.start(),
        svlen = refRange.end() - refRange.start() + 1, variantDbId = "${refRange.id()}",
        variantNames = listOf("RefRange_${refRange.id()}"), variantSetDbId = listOf(variantSetDbId), // TODO need to figure out a better way to get all variant set dbs
        variantType = "REF_RANGE", alternateBases = altAlleles)
}

/**
 * Simple function to get a list of the nodes at a reference range, sort them for consistency, and convert the node to an id.
 * This is needed for the AltAlleleList in the Variant Response.
 */
fun buildAltAlleleList(refRange: ReferenceRange, graph:HaplotypeGraph) : List<String> {
    //Get the nodes
    return graph.nodes(refRange).sortedBy { it.id() }.map { "${it.id()}" }
}