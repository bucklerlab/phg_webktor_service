package api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import model.EdgeListResponse
import model.ServerInfoResponse

class ServerInfoTest : StringSpec({

    "Test /serverinfo from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/serverinfo"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        val serverInfoResult : ServerInfoResponse = mapper.readValue(result)

        val serverInfo = serverInfoResult.result


        val endpoints = serverInfo.calls
        //check number of nodes
        endpoints.size shouldBe 22
    }
})