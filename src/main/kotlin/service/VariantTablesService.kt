package service

import model.TokenPagination
import model.Variant
import model.VariantTable
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.ReferenceRange
import org.apache.log4j.Logger
import utilities.convertReferenceRangeToVariant
import utilities.filterGraphByRequestedRanges
import utilities.parseRangeString
import java.util.*
import kotlin.system.measureNanoTime

class VariantTablesService {

    private val myLogger = Logger.getLogger(VariantTablesService::class.java)

    fun getBigDummyTable() : VariantTable {
        val genotypes = listOf<String>("0/0","1/1","0/1","1/0")
        val finalGenotypes = mutableListOf<List<String>>()
        val rand = Random(12345)

//        val numberRows = 50000
        val numberRows = 500
        val numberCols = 5000
        val timeSpent = measureNanoTime {
            for (rowCount in 0 until numberRows) {
                val rowList = mutableListOf<String>()
                for (colCount in 0 until numberCols) {
                    rowList.add(genotypes[rand.nextInt(4)])
                }
                finalGenotypes.add(rowList)
            }
        }

        println("Time Spent making big table: ${timeSpent/1E9}")

        return VariantTable(finalGenotypes)
    }

    fun buildGraphAndGetTable(methodName: String, pageSize: Int, currentPage: Int = 0, rangeString : String="", taxaToKeep: String = "") : Pair<TokenPagination?,VariantTable?> {
        val graph = buildGraph(methodName) ?: return Pair(null,null)
        return getTableFromGraph(graph, pageSize, currentPage, rangeString, taxaToKeep)
    }

    fun getTableFromGraph(graph: HaplotypeGraph, pageSize: Int, currentPage: Int = 0, rangeString : String="", taxaToKeep: String = "") : Pair<TokenPagination,VariantTable> {
        val totalMethodStTime = System.nanoTime()
        //paginating by referenceRange
        val rangesToKeep = if(rangeString.isEmpty()) listOf<ReferenceRange>() else parseRangeString(rangeString)

        val refRangeFilterStTime = System.nanoTime()
        val referenceRanges = filterGraphByRequestedRanges(graph.referenceRangeList().toList().sorted(), rangesToKeep)
        val refRangeFilterEndTime = System.nanoTime()


        val keepTaxa = taxaToKeep
            .split(",")
            .map { it.trim() }
            .filter { !it.isEmpty() }
            .toSortedSet()

        val chunkStTime = System.nanoTime()

        val referenceRangesChunked = referenceRanges.chunked(pageSize)

        val chunkEndTime = System.nanoTime()
        check(currentPage < referenceRangesChunked.size) { "Requested page is unavailable." }
        val currentSetOfRanges = referenceRangesChunked[currentPage]

        val taxonStTime = System.nanoTime()
        val taxonListMap = graph.taxaInGraph().sorted()
            .filter { keepTaxa.isEmpty() || keepTaxa.contains(it.name) }
            .mapIndexed { index, taxon -> Pair(taxon.name, index) }
            .toMap()

        val taxonEndTime = System.nanoTime()

        val numberOfTaxon = taxonListMap.size
        val taxonKeys = taxonListMap.keys.toHashSet()
        val outputGenotypes = mutableListOf<List<String>>()

        var totalSortTime = 0L
        var totalIndexTime = 0L
        var totalBuildListTime = 0L
        for(refRange in currentSetOfRanges) {
            //Get the nodes
            val sortStTime = System.nanoTime()
            val nodesInRange = graph.nodes(refRange).sortedBy { it.id() }
            totalSortTime += (System.nanoTime() - sortStTime)
            //Read them into taxon -> node map
            val outputCallList = (0 until numberOfTaxon).map { "./." }.toMutableList()

            //We are shifting the index for the gentoype call up by 1 in order to closely match hVCF format.
            //hVCF format is used by PHG to represent the Haplotype Matrix in a VCF like format
            //The main difference is that each SNP is a RefRange and we use symbolic alleles in the ALT column to represent individual haplotypes
            val indexStTime = System.nanoTime()
            val nodeToIndexMap = nodesInRange.mapIndexed{ index, haplotypeNode -> Pair(haplotypeNode.id(), index+1) }.toMap()
            totalIndexTime+=(System.nanoTime() - indexStTime)

            val buildListStTime = System.nanoTime()
            for (node in nodesInRange) {
                val nodeString = "${nodeToIndexMap[node.id()]}/${nodeToIndexMap[node.id()]}"
                for(taxon in node.taxaList()) {
                    val name = taxon.name
                    if(taxonKeys.contains(name)) {
                        val taxonIndex = taxonListMap[name]
                        outputCallList[taxonIndex!!] = nodeString
                    }
                }
            }

            totalBuildListTime += (System.nanoTime() - buildListStTime)
            outputGenotypes.add(outputCallList)
        }

        val nextPageToken = if(currentPage +1 < referenceRangesChunked.size) "${currentPage + 1}" else null
        val prevPageToken = if(currentPage != 0) "${currentPage - 1}" else null
        var pagination = TokenPagination(pageSize=pageSize, nextPageToken=nextPageToken, currentPageToken="$currentPage",
            totalCount=referenceRanges.size, totalPages=referenceRangesChunked.size, prevPageToken = prevPageToken)
        val totalMethodEndTime = System.nanoTime()

        println("Total Method Time: ${(totalMethodEndTime - totalMethodStTime)/1E9} Seconds.")
        println("Total Sort RefRangeTime: ${(refRangeFilterEndTime-refRangeFilterStTime)/1E9} Seconds.")
        println("Total Chunked Time: ${(chunkEndTime-chunkStTime)/1E9} Seconds.")
        println("Total Taxon Filter Time: ${(taxonEndTime-taxonStTime)/1E9} Seconds.")
        println("Total Sort Time: ${totalSortTime/1E9} Seconds.")
        println("Total Index Time: ${totalIndexTime/1E9} Seconds.")
        println("Total Build List Time: ${totalBuildListTime/1E9} Seconds.")

        return Pair(pagination, VariantTable(outputGenotypes))
    }

    /**
     * This function builds a variant list based on the reference ranges in a graph.
     * It queries for the cached graph, pulls the reference ranges, and sends back
     * the "slice" requested based on the specified pagination parameters.
     *
     * NOTE: This currently supports token based pagination, but is using the page Index as the token.
     *
     * This effectively is making it index based pagination, just with the additional nextToken and prevToken fields.
     * Both the nextToken and prevToken fields refer to page indices.
     * If the user does not request a page, page=0 will be assumed.  The next pages will be page=1, page=2 and so on until page == pageSize
     */
    fun generateVariantsListFromGraph(graphDbId: String, currentPageToken: Int, pageSize: Int, rangeString : String=""): Pair<TokenPagination,List<Variant>> {
        val graph = buildGraph(graphDbId)

        val rangesToKeep = if(rangeString.isEmpty()) listOf<ReferenceRange>() else parseRangeString(rangeString)
        var totalPages = 0
        if (graph == null ) {
            myLogger.warn("GRAPH IS NULL - generateVariantsListFromGraph could not get or generate graph!!!!")
            // next page size is null
            return Pair(TokenPagination(pageSize = pageSize, currentPageToken = currentPageToken.toString(), totalPages = totalPages,nextPageToken=null), emptyList())
        }

        val endId = currentPageToken + pageSize -1; // endId is inclusive
        val rangesAfterFilter = filterGraphByRequestedRanges(graph.referenceRangeList(), rangesToKeep)
        // This calculation rounds up
        totalPages = (rangesAfterFilter.size + pageSize - 1)/pageSize

        val referenceRangesChunked = rangesAfterFilter.chunked(pageSize)
        check(currentPageToken < referenceRangesChunked.size) { "Requested page is unavailable." }
        val referenceRangesForPage = referenceRangesChunked[currentPageToken]



//        println("generateVariantsListFromGraph - currentPageToken:${currentPageToken}, endId=${endId}")
//        // Get list of ranges based on user page request
//        val referenceRangesForPage = rangesAfterFilter
//            .filter {it.id() in currentPageToken .. endId}

        if (referenceRangesForPage == null || referenceRangesForPage.size < 1 ) {
            // next page size is null.
                 myLogger.warn("VariantTablesService - nothing left after filtering the reference ranges per user page request!")
            return Pair(TokenPagination(pageSize = pageSize, currentPageToken = currentPageToken.toString(), totalPages = totalPages,nextPageToken=null), emptyList())
        }

        // create Variant objects from the filtered reference range list
        val allVariantsList = referenceRangesForPage.map{ refRange  ->
            convertReferenceRangeToVariant(refRange, graphDbId,graph)
        }

//        var nextPageToken: String? = (endId+1).toString()
//        if (endId  >= rangesAfterFilter.size) {
//            // no nextPageToken
//            nextPageToken = null
//        }
//
//        var pagination = TokenPagination(pageSize=pageSize, nextPageToken=nextPageToken, currentPageToken=currentPageToken.toString(), totalCount=rangesAfterFilter.size, totalPages=totalPages)
//
        val nextPageToken = if(currentPageToken +1 < referenceRangesChunked.size) "${currentPageToken + 1}" else null
        val prevPageToken = if(currentPageToken != 0) "${currentPageToken - 1}" else null
        var pagination = TokenPagination(pageSize=pageSize, nextPageToken=nextPageToken, currentPageToken="$currentPageToken",
            totalCount=rangesAfterFilter.size, totalPages=referenceRangesChunked.size, prevPageToken = prevPageToken)

        return Pair<TokenPagination, List<Variant>>(pagination,allVariantsList)
    }


}