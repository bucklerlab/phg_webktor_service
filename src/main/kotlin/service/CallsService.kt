package service

import model.Call
import model.ListValue
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import org.apache.log4j.Logger

/**
 * Static function to create a Taxon Name to Genotype DB id.  This is helpful to get out the DBId directly from the name.
 */
fun getTaxonToGenoIdMap() : Map<String,Int> {
    val sqlStatement = "SELECT distinct line_name, genoid " +
            "FROM genotypes;"

    println("allTaxaNames: query statement: $sqlStatement")

    // The connection is automatically close when this block finishes.
    DataSource.connection.use { connection ->
        try {
            val result = mutableMapOf<String, Int>()
            connection.createStatement().executeQuery(sqlStatement).use { rs ->

                val time = System.nanoTime()

                while (rs.next()) {

                    val sampleName = rs.getString(1)
                    val sampleDbId = rs.getInt(2)
                    result[sampleName] = sampleDbId
                }

                println("allTaxaNames: number of taxa lists: ${result.size}")
                println("""allTaxaNames: time: ${(System.nanoTime() - time) / 1e9} secs.""")

                return result

            }
        } catch (e: Exception) {
            println(e.message)
            e.printStackTrace()
            throw IllegalStateException("SamplesService: allTaxaNames: Problem querying the database: ${e.message}")
        }

    }
}

/**
 * This service accesses a PHG db instance to return data
 * relating to Calls as defined by the BrAPI interface specification.
 * For the PHG, "calls" are haplotypes from the haplotypes table.
 * It is still a work in progress.
 */
class CallsService {
    private val myLogger = Logger.getLogger(CallsService::class.java)

    /**
     * Function to create a taxon -> genotypeId mapping from the DB.
     *
     * TODO rewrite this to not need a graph, but rather hit the DB for all the genotypes.
     */
    fun getTaxonToGenoIdMap(graph: HaplotypeGraph, phgDBAccess: PHGdbAccess): Map<String, Int> {
        return graph.taxaInGraph().map { it.name }
                .associate { Pair(it, phgDBAccess.getGenoidFromLine(it)) }
    }

    /**
     * Function to build a call list from a graph.
     *
     */
    fun buildCallListFromGraph(): List<Call> {

        // We don't need the DBLoadingUtils.connection as the web interface will ONLY be accessing
        // an existing db.  We will not be creating a new DB here. Get connection to existing db.
        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->

            val phgDBAccess = PHGdbAccess(connection) // TODO would be nice to just get a dump of the table directly.

            val graph = buildGraph("GATK_PIPELINE") ?: return listOf()

            val taxonNameToGenoIdMap = getTaxonToGenoIdMap(graph, phgDBAccess)
            return graph.referenceRanges().flatMap { referenceRange ->
                graph.nodes(referenceRange).sortedBy { it.id() }
                        .flatMap { node ->
                            node.taxaList().map { taxon -> taxon.name }
                                    .map { taxon ->
                                        val listValue = ListValue()
                                        listValue.addValuesItem(node.id().toString())
                                        val call = Call(callSetDbId = "${taxonNameToGenoIdMap[taxon]}", callSetName = taxon, genotype = listValue,
                                                        variantDbId = "${referenceRange.id()}", variantName = "RefRange_${referenceRange.id()}" )
                                        call
                                    }
                        }
            }.toList()

        }
    }

    /**
     * Function to build a list of calls from a graph.
     * Need to first loop through the nodes then through each taxa.
     */
    fun buildCallListFromGraph(graph: HaplotypeGraph) : List<Call> {
        val taxonNameToGenoIdMap = getTaxonToGenoIdMap()
        return graph.referenceRanges().flatMap { referenceRange ->
            graph.nodes(referenceRange).sortedBy { it.id() }
                    .flatMap { node ->
                        node.taxaList().map { taxon -> taxon.name }
                                .map { taxon ->
                                    val listValue = ListValue()
                                    listValue.addValuesItem(node.id().toString()) //Need to add in the node id 2 times as it is diploid.
                                    listValue.addValuesItem(node.id().toString())
                                    val call = Call(callSetDbId = "${taxonNameToGenoIdMap[taxon]}", callSetName = taxon, genotype = listValue,
                                            variantDbId = "${referenceRange.id()}", variantName = "RefRange_${referenceRange.id()}" )
                                    call
                                }
                    }
        }.toList()
    }

    /**
     * Simple function to build a List of calls from the created callList.
     *
     * callList is a list of triples.  Each triple contains the callsetId, the variantId and the list of calls.
     * For haplotypes, this is just a list of HaplotypeIds.
     *
     * We need to have the callsetIdToTaxon map to get the correct callset name.
     * Otherwise we will not be able to fill out the callSetName for the Call.
     */
    fun buildCallsFromCallList(variantSetDbId : String, callList : List<Triple<Int, Int, List<Int>>>, callSetIdToTaxonMap : Map<Int, String>) : List<Call> {

        return callList.map {
            val callSetId = it.first
            val variantId = it.second
            val haplotypes = it.third

            val listValue = ListValue()

            for(haplotype in haplotypes) {
                listValue.addValuesItem(haplotype.toString())
            }

            //If its haploid add it twice.
            if(haplotypes.size == 1) {
                listValue.addValuesItem(haplotypes.first().toString())
            }

            Call(callSetDbId = "${callSetId}", callSetName = callSetIdToTaxonMap[callSetId]?:"", genotype = listValue,
                    variantDbId = "${variantId}", variantName = "RefRange_${variantId}" )
        }
    }

}