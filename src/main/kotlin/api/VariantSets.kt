package api

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import model.*
import service.VariantSetsService


/**
 * This Route function denotes all the GET Variantset endpoints.
 * In the PHG we handle would like to handle both SNP and HAP versions of the Variantsets, but for now we only return
 * haplotype calls.  The user can specify this by providing the correct variantSetDbId
 *
 * The variantSetDbId in this case represents a method name in the PHG.  We can handle both Haplotype and Path methods.
 * If the user wants to bring out the SNPs, they will need to provide SNP_methodName as the variantSetDbId.
 * If they would like haplotypes they will need to provide either HAP_methodName or just methodName.
 *
 * Each of these will first build a graph based on the provided method and will then extract out the needed information
 * depending on the endpoint.  We also implement caching so we speed up the graph creation.
 */
fun Route.variantSets() {
    route("/variantsets") {

        val variantSetsService = VariantSetsService()

        //Endpoint to get all the variantsets in the DB.
        //This can be used to get the list of all the ids to further get more information using the other endpoints.
        //This will also get the dimensions for all the variant sets in the DB.
        get("") {
            call.respond(
                VariantSetsListResponse(
                    Metadata(),
                    VariantSetsListResponseResult(variantSetsService.generateVariantSets().toTypedArray())
                )
            )
        }

        //This end point will return a specific variantSet corresponding to a variantSetDbId.
        //If the id is not found a 404 will be thrown.
        get("/{variantSetDbId}") {
            val variantSetDbId = call.parameters["variantSetDbId"] ?: throw IllegalStateException("Must provide variantSetDbId")
            val variantSet = variantSetsService.generateVariantSetsFromMethodName(variantSetDbId)

            if(variantSet == null) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested VariantSet object ${variantSetDbId} was not found in the database")
            }
            else {
                call.respond(VariantSetResponse(Metadata(), variantSet))
            }
        }

        //This end point will return all the calls corresponding to a specific variantSetDbId.
        //If the id is not found, a 404 will be thrown.
        get("/{variantSetDbId}/calls") {
            val variantSetDbId = call.parameters["variantSetDbId"] ?: throw IllegalStateException("Must provide variantSetDbId")

            val pageNumber = call.parameters["page"]?.toInt() ?: 0
            val pageSize = call.parameters["pageSize"]?.toInt() ?: defaultCallPageSize


            if(pageNumber < 0) {
                call.respond(" ${HttpStatusCode.NotFound}: The page number is negative.  It needs to be 0 or higher.")
            }

            val callsAndPagination = variantSetsService.generateCallsFromMethodName(variantSetDbId, pageNumber, pageSize)

            val pagination = callsAndPagination.first
            val calls = callsAndPagination.second

            if(calls.isEmpty()) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested Calls objects corresponding to  ${variantSetDbId} was not found in the database")
            }
            else {
                var metadata = MetadataTokenPagination(pagination=pagination)
                call.respond(CallsListResponse(metadata, CallsListResponseResult(calls.toTypedArray())))
            }

        }

        //This endpoint will return all the callsets(taxa) in the DB corresponding to a specific variantSetDbId.
        //If the id is not found a 404 will be thrown.
        get("/{variantSetDbId}/callsets") {
            val variantSetDbId = call.parameters["variantSetDbId"] ?: throw IllegalStateException("Must provide variantSetDbId")

            val callsets = variantSetsService.generateCallsetsFromMethodName(variantSetDbId)
            if (callsets.isEmpty()) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested CallSet objects corresponding to  ${variantSetDbId} was not found in the database")
            }
            else {
                call.respond(CallSetsListResponse(Metadata(), CallSetsListResponseResult(callsets.toTypedArray())))
            }

        }

        //This endpoint will return all the variants(reference ranges or VCF positions) in the DB corresponding to a specific variantSetDbId
        //If the id is not found a 404 will be thrown.
        get("/{variantSetDbId}/variants") {
            val variantSetDbId = call.parameters["variantSetDbId"] ?: throw IllegalStateException("Must provide variantSetDbId")

            val variants = variantSetsService.generateVariantsFromMethodName(variantSetDbId)

            if (variants.isEmpty()) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested Variants objects corresponding to  ${variantSetDbId} was not found in the database")
            }
            else {
                call.respond(VariantsListResponse(MetadataTokenPagination(), VariantsListResponseResult(variants.toTypedArray())))
            }
        }
    }
}