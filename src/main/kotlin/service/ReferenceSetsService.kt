package service

import model.ReferenceSet
import net.maizegenetics.plugindef.Plugin.myLogger
import org.apache.log4j.Logger
import java.util.stream.Collectors

/**
 * For PHG, ReferenceSets represent the Genome.  By contrast, Reference is a Chromosome
 *
 * We are only filling in these fields in the table:
 *  mdchecksum:  file_checksum from the genome_file_data table
 *  referenceSetDbId: id from the genome_file_data table
 *  referenceSetName: name of the reference from genotypes line_name field
 *  sourceURI: from genome_file_data:  genome_path/genome_file
 *  description:  a list of chromosome names from the PHG reference_ranges table
 */
class ReferenceSetsService {
    private val myLogger = Logger.getLogger(ReferenceSetsService::class.java)

    /**
     * This gets data from the PHG genome_file_data and reference_ranges tables,
     * then translates to fields in the ReferenceSet model
     * It currently only gets data for the reference line.
     *
     * Should this be expanded to get reference data for both the PHG reference and
     * the assemblies? Getting reference data for a particular ID allows the user to
     * get data for one of the assemblies vs the reference.
     */
    fun getReferenceGenomeData(): List<ReferenceSet> {

        val time = System.nanoTime()
        val chromList = getChromNames()

        // Get reference line name
        val sqlStatement = "SELECT  line_name from genotypes where is_reference=1"
        myLogger.info("getReferenceGenomeData: query ref line name statement: ${sqlStatement}")

        var refLine:String? = null
        DataSource.connection.use { connection ->
            try {
                connection.createStatement().executeQuery(sqlStatement).use { rs ->
                    if (rs.next()) {
                        refLine = rs.getString("line_name")
                    }
                }
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                exc.printStackTrace()
                throw IllegalStateException("ReferenceSetService: getReferenceGenomeData: Problem querying the database for reference line name: ${exc.message}")
            }
        }

        if (refLine == null) {
            // This should never happen - software sets the reference line when populating PHG reference data
            myLogger.warn("ReferenceSetService: getReferenceGenomeData: no reference line found in the database")
            return listOf<ReferenceSet>()
        }
        val chromString: String = chromList.joinToString(",")
        val result = getGenomeData(refLine!!, chromString)
        val resultSet = if(result != null) listOf<ReferenceSet>(result) else listOf<ReferenceSet>()

        myLogger.info("getReferenceGenomeData:  processed in ${(System.nanoTime() - time) / 1e9} secs.")
        return resultSet
    }

    /**
     * Gets data from the genome_file_data table based on a
     * taxon's line_name from the genotypes table
     */
    fun getGenomeDataForId(id:String): ReferenceSet {

        val time = System.nanoTime()
        val chromList = getChromNames()

        val chromString: String = chromList.joinToString(",")
        val result = getGenomeData(id, chromString)

        myLogger.info("getReferenceGenomeData:  processed in ${(System.nanoTime() - time) / 1e9} secs.")
        return result
    }



    /**
     * for each PHG db, there will only be 1 reference set.  If a user wanted to get information
     * relating to a specific assembly, they could pass in that assembly's db name to see what
     * was in the table.  The chromosome names will still be the same names stored in the reference_ranges table
     */
    fun getGenomeData(id:String, chromString: String): ReferenceSet {
        val sqlStatement = "SELECT  gt.line_name, gfd.id, gfd.genome_path, gfd.genome_file, gfd.file_checksum " +
                "FROM genotypes  gt, genome_file_data gfd " +
                "WHERE gt.genoid=gfd.genoid AND gt.line_name= '" + id + "'";

        myLogger.info("getReferenceGenomeData: query genome data statement: ${sqlStatement}")
        //val resultSet = mutableListOf<ReferenceSet>()
        var result = ReferenceSet()
        DataSource.connection.use { connection ->
            try {
                connection.createStatement().executeQuery(sqlStatement).use { rs ->
                    if (rs.next()) {
                        //val id = rs.getInt("id")
                        val sourceURI = "${rs.getString("genome_path")}/${rs.getString("genome_file")}"
                        result = ReferenceSet(md5checksum = rs.getString("file_checksum"), referenceSetName = rs.getString("line_name"), referenceSetDbId = rs.getInt("id").toString(), sourceURI=sourceURI,description = chromString)
                    }
                }
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                exc.printStackTrace()
                throw IllegalStateException("ReferenceSetService: getReferenceGenomeData: Problem querying the database: ${exc.message}")
            }
        }
        return result
    }
}
fun getChromNames():List<String> {
    // get a list of chromosomes names from the reference_ranges table
    var sqlStatement = "SELECT  distinct(chrom) from reference_ranges";
    val chromList = mutableListOf<String>()
    DataSource.connection.use { connection ->
        try {
            connection.createStatement().executeQuery(sqlStatement).use { rs ->
                while (rs.next()) {
                    val chrom = rs.getString("chrom")
                    chromList.add(chrom)
                }
                myLogger.info("ReferenceSetService: getReferenceGenomeData: number of chromosomes: ${chromList.size} ")
            }
        } catch (exc: Exception) {
            myLogger.debug(exc.message, exc)
            exc.printStackTrace()
            throw IllegalStateException("ReferenceSetService: getReferenceGenomeData: Problem querying the database for chromosomes: ${exc.message}")
        }
    }

    val chromString: String = chromList.stream().collect(Collectors.joining(","))
    return chromList
}