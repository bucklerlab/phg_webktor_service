package service

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.server.config.*
import java.sql.Connection
import java.sql.SQLException

/**
 * This object creates a HikariCP connection pooling source.
 * It controls the connections for the data base.
 *
 * When a connection is needed, it is retrieved from the pool via
 * a DataSource.connection call
 *
 * @author lcj34
 */
object DataSource {
    private var ds: HikariDataSource? = null

    @get:Throws(SQLException::class)
    val connection: Connection
        get() = ds!!.connection

    init {
            ds = getDataSource()
    }

    // Setup the HikariDataSource.  ConfigFactory looks for application.conf file in the resources folder
    fun getDataSource(): HikariDataSource {
        val appConfig = HoconApplicationConfig(ConfigFactory.load())
        val jdbcURL = appConfig.property("db.jdbcUrl").getString()
        val userName = appConfig.property("db.dbUser").getString()
        val password = appConfig.property("db.dbPassword").getString()

        val config = HikariConfig()

        config.setJdbcUrl(jdbcURL)
        config.setUsername(userName)
        config.setPassword (password)

        // See https://www.baeldung.com/hikaricp or HikariCP documentation for other properties that can be set
        // See https://github.com/brettwooldridge/HikariCP/wiki/About-Pool-Sizing for good video on sizing of
        // connection pools
        config.maximumPoolSize = 5 // what should this be?
        config.isAutoCommit = false

        config.addDataSourceProperty("cachePrepStmts", "true")
        config.addDataSourceProperty("prepStmtCacheSize", "250")
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048")
        config.validate()

        return HikariDataSource(config)
    }
}
