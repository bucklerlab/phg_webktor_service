package service

import com.typesafe.config.ConfigFactory
import io.ktor.server.config.*
import io.ktor.util.*
import model.Graph
import model.GraphDbIdModel
import model.VariantTableMetaData
import net.maizegenetics.pangenome.api.BuildGraphFromPathsPlugin
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.taxa.TaxaList
import net.maizegenetics.taxa.TaxaListBuilder
import org.apache.log4j.Logger
import org.ehcache.config.builders.CacheConfigurationBuilder
import org.ehcache.config.builders.CacheManagerBuilder
import org.ehcache.config.builders.ResourcePoolsBuilder
import org.ehcache.core.internal.statistics.DefaultStatisticsService
import org.ehcache.core.spi.service.StatisticsService
import utilities.getPHGMethodNamesForMethodType
import java.util.*


// Set up variables from the Ktor HCOCON (Human-Optimized Config Object Notation) file
private val config = HoconApplicationConfig(ConfigFactory.load())

@KtorExperimentalAPI
val phgConfigFile = config.property("phgConfigFile").getString()

private val myLogger = Logger.getLogger(GraphService::class.java)

val statisticsService: StatisticsService = DefaultStatisticsService()

/**
 * We are caching the Graphs here.  The String key is an identifier built when querying.  When querying by method name, the method name is the key.
 * If querying by haplotypeId, a generated string is used of HapIds_+eachHapIdSepByUnderscore.
 * This might be better hashed so the length is shorter but for now we are keeping it.
 */
private val cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
    .using(statisticsService)
    .withCache(
        "graphCache",
        CacheConfigurationBuilder.newCacheConfigurationBuilder(
            String::class.java,
            HaplotypeGraph::class.java,
            ResourcePoolsBuilder.heap(100).build()
        )
    )
        .build(true)

private val graphCache = cacheManager.getCache("graphCache",String::class.java, HaplotypeGraph::class.java)

enum class VariantTypeEnum(val value: String) {
    HAP("HAP"), SNP("SNP")
}

val methodTypeEnums = DBLoadingUtils.MethodType.values()
        .map { Pair(it.value, it) }
        .toMap()

//val variantSetDbIdToHapType  = getAllDBIdsAndTypes().map { Pair(it.first,methodTypeEnums[it.second.toInt()]) }.toMap()
val variantSetDbIdToHapType  = getAllDBIdsAndTypes().map { Pair(it.first,it.second) }.toMap()


object GraphService {

    init {

        val methods = getPHGMethodNamesForMethodType(
            listOf(
                DBLoadingUtils.MethodType.PATHS.value,
                DBLoadingUtils.MethodType.ASSEMBLY_HAPLOTYPES.value,
                DBLoadingUtils.MethodType.ANCHOR_HAPLOTYPES.value,
                DBLoadingUtils.MethodType.CONSENSUS_ANCHOR_SEQUENCE.value
            )
        )

        methods.forEach { method ->
            println("building haplotype graph for method: $method")
            // builds graph which puts it into the cache.
            // runs taxaInGraph() to cache resulting taxa list for future requests.
            buildGraph(method)?.taxaInGraph()
            val ehCacheStat = statisticsService.getCacheStatistics("graphCache");
            println("haplotype graph cache size: ${ehCacheStat.getTierStatistics().get("OnHeap")?.getMappings()}")
        }

    }

    fun getAllGraphs(): List<Graph> {
        val methodIds = getMethodIdsFromDB()

        return methodIds.asSequence() //Casting to sequence so its lazy
            .map { buildAPIGraphForId(it) }
            .filterNotNull()
            .toList()
    }

    fun buildAPIGraphForId(graphDbId : String) :Graph? {
        val graph = buildGraph(graphDbId) ?: return null

        val nodeService = NodeService()
        val nodes = nodeService.getAllNodesForGraph(graph)

        val edgeService = EdgeService()

        val edges = edgeService.getAllEdgesFromGraph(graph)


        return Graph(graphDbId, "DirectedGraph",nodes, edges)

    }

    fun getAllGraphDbIdModel(): List<GraphDbIdModel> {
        val methodIdsAndTypes = getAllDBIdsAndTypes()

        return methodIdsAndTypes.map { GraphDbIdModel(it.first, it.second) }
    }

}

fun variantTablesMetaData(): Array<VariantTableMetaData> {

    val result = mutableListOf<VariantTableMetaData>()

    graphCache.forEach {

        val id = it.key
        val graph = it.value
        val numReferenceRanges = graph.referenceRanges().size
        val numberOfTaxon = graph.taxaInGraph().size
        result.add(VariantTableMetaData(id, numReferenceRanges, numberOfTaxon))

    }

    return result.toTypedArray()

}

fun variantTablesMetaData(variantTableDbId: String): VariantTableMetaData {

    val graph = graphCache[variantTableDbId]
    val numReferenceRanges = graph.referenceRanges().size
    val numberOfTaxon = graph.taxaInGraph().size
    return VariantTableMetaData(variantTableDbId, numReferenceRanges, numberOfTaxon)

}

/**
 * Generic function to build graphs.  This will allow for making a graph from paths or from haplotype methods.  This will figure out the type and call the correct builder.
 */
fun buildGraph(
    methods: String, includeSequences: Boolean = false,
    includeVariantContexts: Boolean = false,
    taxaList: TaxaList = TaxaListBuilder.getInstance(0),
    hapIds: SortedSet<Int> = sortedSetOf<Int>()
): HaplotypeGraph? {
    if (graphCache.containsKey(methods)) return graphCache[methods]

    val graph = when (variantSetDbIdToHapType[methods]) {
        DBLoadingUtils.MethodType.PATHS.name -> buildGraphFromPathMethod(methods)
        DBLoadingUtils.MethodType.ANCHOR_HAPLOTYPES.name,
        DBLoadingUtils.MethodType.ASSEMBLY_HAPLOTYPES.name,
        DBLoadingUtils.MethodType.CONSENSUS_ANCHOR_SEQUENCE.name -> buildHapGraph(
            methods,
            includeSequences,
            includeVariantContexts,
            taxaList,
            hapIds
        )
        else -> {
            return null
        }
    }
    if(graph != null) {
        graphCache.put(methods, graph)
    }

    myLogger.info("Done building graph")
    return graph
}

/**
 * Generic function to build a graph.  This should be called by all Services to build a graph.
 *
 * If taxaList or hapIds are empty, all taxa and hapIds will be used respectively.
 *
 * Defaults of taxaList and hapIds are just empty collections.
 */
fun buildHapGraph(methods: String, includeSequences : Boolean = false,
               includeVariantContexts : Boolean = false,
               taxaList: TaxaList = TaxaListBuilder.getInstance(0),
               hapIds: SortedSet<Int> = sortedSetOf<Int>()) : HaplotypeGraph? {

    check(phgConfigFile != null) { "application.conf file must provide phgConfigFile variable" }

    val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(phgConfigFile)
            .methods(methods)
            .includeSequences(includeSequences)
            .includeVariantContexts(includeVariantContexts)

    if(!hapIds.isEmpty()) {
        println("WARN hapIds is not specified, using all hapIds in graphBuilder.")
        graphBuilder.hapids(hapIds)
    }

    if (taxaList.size != 0) {
        graphBuilder.taxaList(taxaList)
    } else {
        println("WARN taxaList is not specified, using all taxa in graphBuilder.")
    }

    return try {
        return graphBuilder.build()
    } catch (exc: Exception) {
        null
    }
}

/**
 * Generic function to build a haplotype graph from a path method.
 * If the taxaList is empty, all taxa will be used.
 *
 * TODO implement includeSequence and VariantContexts, but this is waiting on BuildGraphFromPathsPlugin to implement first.
 */
fun buildGraphFromPathMethod(pathMethod : String, includeSequences: Boolean = false,
                             includeVariantContexts : Boolean = false,
                             taxaList: TaxaList = TaxaListBuilder.getInstance(0)) : HaplotypeGraph? {

    check(phgConfigFile != null) { "application.conf file must provide phgConfigFile variable" }

    ParameterCache.load(phgConfigFile)


    val graphBuilder = BuildGraphFromPathsPlugin().pathMethod(pathMethod)

    if (taxaList.size != 0) {
        graphBuilder.pathTaxaList(taxaList)
    } else {
        println("WARN taxaList is not specified, using all taxa in graphBuilder.")
    }

    return try {
        val graph = graphBuilder.build()

        if(graph.numberOfNodes() == 0) return null
        return graph
    } catch(exc: Exception) {
        println(exc)
        null
    }



}

fun parseTypeAndMethodName(dbMethodName : String) : Pair<VariantTypeEnum, String> {
    //split on first underscore
    val type = dbMethodName.substringBefore("_")
    val typeEnum = getTypeEnum(type)
    return Pair(typeEnum, getMethodName(type, dbMethodName))
}

fun getTypeEnum(type:String) : VariantTypeEnum {
    return when (type) {
        "SNP" -> VariantTypeEnum.SNP
        "HAP" -> VariantTypeEnum.HAP
        else -> {
            VariantTypeEnum.HAP
        }
    }
}

fun getMethodName(type: String, dbMethodName : String) : String {
    return when(type) {
        "SNP", "HAP" -> dbMethodName.substringAfter("_")
        else -> {
            dbMethodName
        }
    }
}

fun buildGraphByHapIds(hapIds: SortedSet<Int>, includeSequences : Boolean = false,
                       includeVariantContexts : Boolean = false,
                       taxaList: TaxaList = TaxaListBuilder.getInstance(0)) :HaplotypeGraph {

    val cacheKey = "HapIds_${hapIds.joinToString("_")}"
    if(graphCache.containsKey(cacheKey)) return graphCache[cacheKey]

    check(phgConfigFile != null) { "application.conf file must provide phgConfigFile variable" }
    val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(phgConfigFile)
            .includeSequences(includeSequences)
            .includeVariantContexts(includeVariantContexts)
            .hapids(hapIds)

    if(taxaList.size != 0) {
        println("WARN taxaList is not specified, using all taxa in graphBuilder.")
        graphBuilder.taxaList(taxaList)
    }
    val graph = graphBuilder.build()

    graphCache.putIfAbsent(cacheKey, graph)

    return graph

}

fun getMethodIdsFromDB() : List<String> {
    val idsAndTypes = getAllDBIdsAndTypes()
    return idsAndTypes.map { it.first }
}

fun getAllDBIdsAndTypes() : List<Pair<String,String>> {
    val sqlStatement = "SELECT distinct name, method_type " +
            "FROM methods " +
            "WHERE method_type=1 OR method_type=2 OR method_type=3 OR method_type=6;"
    myLogger.info("getMethodIdsFromDB: query statement: $sqlStatement")

    val methodTypeEnums = DBLoadingUtils.MethodType.values()
            .map { Pair(it.value, it.name) }
            .toMap()

    // The connection is automatically close when this block finishes.
    DataSource.connection.use { connection ->

        try {
            val methodIds = mutableListOf<Pair<String,String>>()
            connection.createStatement().executeQuery(sqlStatement).use { rs ->

                val time = System.nanoTime()

                while (rs.next()) {
                    val typeId = rs.getInt(2)
                    val typeName = methodTypeEnums[typeId]?:""
                    methodIds.add(Pair(rs.getString(1), typeName))
                }

                myLogger.info("getAllDBIdsAndTypes: number of methods: ${methodIds.size}")
                myLogger.info("""getAllDBIdsAndTypes: time: ${(System.nanoTime() - time) / 1e9} secs.""")

                return methodIds
            }
        } catch (e: Exception) {
            myLogger.debug(e.message, e)
            e.printStackTrace()
            throw IllegalStateException("NodeService: getAllDBIdsAndTypes: Problem querying the database: ${e.message}")
        }
    }
}