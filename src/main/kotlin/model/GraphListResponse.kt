package model

data class GraphListResponse(
        val metadata: Metadata,
        val result: GraphListResponseResult,
        val _atContext: Context? = null
)

data class GraphListResponseResult(val data: Array<Graph>)