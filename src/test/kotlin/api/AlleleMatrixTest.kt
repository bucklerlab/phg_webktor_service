package api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.typesafe.config.ConfigFactory
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.ktor.server.config.*
import model.AlleleMatrixResponse
import model.CallSetsListResponse
import model.VariantsListResponse
import net.maizegenetics.pangenome.api.BuildGraphFromPathsPlugin
import net.maizegenetics.plugindef.ParameterCache

class AlleleMatrixTest: StringSpec({

    "Test alleleMatrix vs Graph" {
        val ktorConfig = HoconApplicationConfig(ConfigFactory.load())
        val phgConfigFile = ktorConfig.property("phgConfigFile").getString()

        ParameterCache.load(phgConfigFile)
        val graph = BuildGraphFromPathsPlugin().pathMethod("GATK_PIPELINE_PATH").build()

        val refRangeToTaxaToCallMap = graph.referenceRanges().map { Pair(it, graph.nodes(it)) }.associate {
            Pair(
                it.first.id(),
                it.second.associate { node -> Pair(node.taxaList().first().name, node.id()) })
        }

        //build the alleleMatrix from the config using method name GATK_PIPELINE_PATH
        val query = "http://localhost:8080/brapi/v2/allelematrix?variantSetDbId=GATK_PIPELINE_PATH"
        val result = khttp.get(query).text
        println("\n Result from test: $result") // lcj test

        val mapper = jacksonObjectMapper()

        val matrixResponse: AlleleMatrixResponse = mapper.readValue(result)
        val matrix = matrixResponse.result

        val variants = matrix.variants
        val taxa = matrix.callSetDbIds
        val calls = matrix.dataMatrices!!.first().dataMatrix


        //Need to get the callset names out using brapi
        val callsetQuery = "http://localhost:8080/brapi/v2/callsets"
        val callsetResult = khttp.get(callsetQuery).text
        val callsetResponse: CallSetsListResponse = mapper.readValue(callsetResult)
        val taxaIdToNameMap = callsetResponse.result.data.associate {it.callSetDbId to it.callSetName }


        //Get out the alternate bases from the variant call
        val variantsQuery = "http://localhost:8080/brapi/v2/variants"
        val variantsResult = khttp.get(variantsQuery).text
        val variantsResponse: VariantsListResponse = mapper.readValue(variantsResult)
        val variantIdToAltMap = variantsResponse.result.data.associate {it.variantDbId to it.alternateBases.mapIndexed { index, s -> Pair(index,s) }.toMap() }


        for(callIdx in taxa.indices) {
            //taxa
            for(variantIdx in variants.indices) {
                //variants
                val call = calls[variantIdx][callIdx].split("/").first().toInt()
                val variantId = variants[variantIdx]
                val altBases = variantIdToAltMap[variantId]!![call]
                val taxonId = taxa[callIdx]
                val taxon = taxaIdToNameMap[taxonId]

                val graphCall = refRangeToTaxaToCallMap[variantId.toInt()]?.get(taxon)

                graphCall shouldBe altBases?.toInt()
            }
        }
    }
})