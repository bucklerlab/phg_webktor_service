package api

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import model.Metadata
import model.NodeListResponse
import model.NodeListResponseResult
import model.NodeResponse
import service.NodeService

fun Route.nodes() {
    val nodeService = NodeService()
    route("/nodes") {

        get("") {
            val nodes = nodeService.getAllNodesForAllGraphs()
            if(nodes.isEmpty()) {
                call.respond(" ${HttpStatusCode.NotFound}: No Nodes found in the DB.")
            }
            else {
                call.respond(NodeListResponse(Metadata(), NodeListResponseResult(nodes.toTypedArray())))
            }

        }

        get("/{nodeDbId}") {
            val nodeDbId = call.parameters["nodeDbId"]?.toInt() ?: throw IllegalStateException("Must provide id")

            val startTime  = System.nanoTime()
            val node = nodeService.getNode(nodeDbId)
            val endTime = System.nanoTime()

            println("Time Spent getting nodes: ${(endTime - startTime)/1e9} seconds.")

            if(node == null) {
                call.respond(" ${HttpStatusCode.NotFound}: the requested Node object ${nodeDbId} was not found in the database.")
            }
            else {
                call.respond(NodeResponse(Metadata(), node))
            }

        }
    }
}