package api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import main
import model.*

class ApiTest: StringSpec({
    // Tried calling "main" here to get the service up before running this test.
    // it DOES call it, but then that thread suspends and it never goes on to
    // run the test.

    // NOTE: If run from continuous integration, attempting to run "main" will fail anyway
    // as the repository does not have values in the application.conf file
    // for DB access. (those must be filled in by the user)
//    val args = arrayOf<String>()
//    main(args)

    // To run these tests as a junit you must first setup
    // your server.  You can either do this from the command line
    // via  "./gradlew -debug run"
    // or from inside your IDE by running "Main.kt:main" of this project
    //  Don't forget to populate the db access variables in the application.conf file
    "Test Calls from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/calls"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        println("\n Result from test: $result")

        val resultAsList: CallsListResponse = mapper.readValue(result)
        println("\n resultAsList size: ${resultAsList.result.data.size}")
        resultAsList.result.data.size shouldBe 60

        // When we add metadata to this the lines above will need to be changed
        // similar to what is done for CallSets and Studies

    }
    "Test CallSets from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/callsets"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        println("\n Result from test: $result")

        val resultAsList: CallSetsListResponse = mapper.readValue(result)
        println("\nresultAsList size: ${resultAsList.result.data.size}")
        // The result is 6 because the method is hard-coded to GATK_PIPELINE
        // this should probably change.
        resultAsList.result.data.size shouldBe 6
    }

    "Test Studies from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/studies"
        val result = khttp.get(query).text
        println("\n Result from test: $result")

        val mapper = jacksonObjectMapper()

        val resultAsList: StudyListResponse = mapper.readValue(result)
        println("\nresultAsList size: \${resultAsList.result.data.size")
        resultAsList.result.data.size shouldBe 9
    }
    "Test Studies By Id from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/studies/7"
        val result = khttp.get(query).text
        println("\n Result from test: $result")

        val mapper = jacksonObjectMapper()
        println("Before resultStudy call - what do I expect here ??")
        val resultStudy: StudySingleResponse = mapper.readValue(result)
        println(resultStudy)

    }
})