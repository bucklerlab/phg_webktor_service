package model

import kotlinx.serialization.Serializable

@Serializable
data class VariantTableMetaData(
    val variantTableDbId: String,
    val numVariants: Int,
    val numSamples: Int,
    val additionalInfo: Map<String, String>? = null
)