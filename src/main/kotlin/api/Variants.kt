package api

import com.typesafe.config.ConfigFactory
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.config.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import model.*
import service.VariantsService

private val config = HoconApplicationConfig(ConfigFactory.load())

/**
 * This Route function denotes all the GET Variants endpoints.
 * In the PHG we would like to handle both SNP and HAP versions of the Variants, but for now we only return
 * haplotype calls.  The user can specify this by providing the correct variantDbId
 *
 * The variantDbId in this case represents a reference range ID in the PHG.  This handles haplotype
 * variants.  IN the future, SNP variants will be supported.
 *
 */
fun Route.variants() {
    route("/variants") {

        val variantService = VariantsService()

        //Endpoint to get all the variants in the DB.
        //This can be used to get the list of all the ids to further get more information using the other endpoints.
        get("") {
            val pageToken = call.parameters["page"]?.toInt() ?: 1 // token is reference range ID, they are stored in order
            val pageSize = call.parameters["pageSize"]?.toInt() ?: defaultVariantsPageSize

            // These are tokens, not array indices.  The ranges are pulled from the database based on
            // reference_range_id BETWEEN start and end.  0 as a start won't cause an error, but it also
            // isn't truly valid.
            if(pageToken < 1) {
                call.respond(" ${HttpStatusCode.NotFound}: Invalid page token: ${pageToken}. The page token represents a reference range id, which is 1 or greater.")
            }

            val variantsAndPagination = variantService.generateVariantsListFromCache(pageToken, pageSize, "all")

            val pagination = variantsAndPagination.first
            val variants = variantsAndPagination.second
            var metadata = MetadataTokenPagination(pagination = pagination)
            call.respond(VariantsListResponse(metadata, VariantsListResponseResult(variants.toTypedArray())))
        }

        //This end point will return data for a specific variant corresponding to a variantDbId.
        //If the id is not found a 404 will be thrown.
        get("/{variantDbId}") {
            val pageToken = call.parameters["page"]?.toInt() ?: 1 // page token is reference range id, which starts at 1
            val pageSize = call.parameters["pageSize"]?.toInt() ?: defaultVariantsPageSize

            if(pageToken < 1) {
                call.respond(" ${HttpStatusCode.NotFound}: The page ${pageToken} is invalid.  It must be 1 or greater.")
            }

            val variantDbId = call.parameters["variantDbId"] ?: throw IllegalStateException("Must provide variantDbId")
            val variant = variantService.generateVariantFromID(variantDbId.toInt(), pageToken, pageSize, "all")

            if(variant == null) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested Variant object ${variantDbId} was not found in the database")
            }
            else {
                // There will only be 1 page, so nextPageToken will be defaulted to null
                call.respond(VariantSingleResponse(MetadataTokenPagination(pagination=TokenPagination(currentPageToken="1")), variant))
            }
        }

        // THis endpoint will return all the calls data for a specific variantId (ie ref_range_id)
        // Per BrAPI spec, this endpoint requires token based paging
        //
        get("/{variantDbId}/calls") {
            val pageToken = call.parameters["page"]?.toInt() ?: 0
            val pageSize = call.parameters["pageSize"]?.toInt() ?: defaultVariantsPageSize

            if(pageToken < 0) {
                call.respond(" ${HttpStatusCode.NotFound}: The page number is negative.  It needs to be 0 or higher.")
            }

            val variantDbId = call.parameters["variantDbId"] ?: throw IllegalStateException("Must provide variantDbId")

            val callListAndNextToken = variantService.generateCallsForVariant(variantDbId.toInt(),pageToken,pageSize)

            val callList = callListAndNextToken.first
            val nextToken = callListAndNextToken.second

            if (callList == null || callList.size == 0) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested calls for Variant ${variantDbId} were not found in the database")
            } else {
                val pagination = TokenPagination(currentPageToken=pageToken.toString(),nextPageToken=nextToken.toString())
                call.respond(CallsListResponse(MetadataTokenPagination(pagination=pagination),CallsListResponseResult( callList.toTypedArray())))
            }
        }
    }
}