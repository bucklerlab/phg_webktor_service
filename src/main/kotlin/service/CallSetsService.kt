package service

import model.CallSet
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import org.apache.log4j.Logger

/**
 * This service makes db queries to a PHG instance and returns
 * information on CallSets as defined by BrAPI.  For PHG purposes,
 * CallSets are a taxa list.
 *
 * It is not fully flushed out.
 */
class CallSetsService {

    private val myLogger = Logger.getLogger(CallSetsService::class.java)


    /**
     * Function to create a taxon -> genotypeId mapping from the DB.
     *
     * TODO rewrite this to not need a graph, but rather hit the DB for all the genotypes.
     */
    fun getTaxonToGenoIdMap(graph: HaplotypeGraph, phgDBAccess: PHGdbAccess): Map<String, Int> {
        return graph.taxaInGraph().map { it.name }
                .associate { Pair(it, phgDBAccess.getGenoidFromLine(it)) }
    }

    /**
     * Function to generate the CallSet List from the graph
     * We can currently set the CallSetDBId, callSetName and sampleDBId.
     *
     * TODO implement the created and updated time stamps and the variant setDBIds
     */
    fun generateCallSetsFromGraph(): List<CallSet> {

        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->

            val phgDBAccess = PHGdbAccess(connection) // TODO would be nice to just get a dump of the tables directly

            val graph = buildGraph("GATK_PIPELINE") ?: return listOf()
            return getTaxonToGenoIdMap(graph, phgDBAccess)
                    .map {
                        val callSet = CallSet()
                        callSet.callSetDbId = "${it.value}"
                        callSet.callSetName = "${it.key}"
                        callSet.sampleDbId = "${it.value}"
                        // callSet.created() TODO add later
                        // callSet.updated() TODO add later
                        // callset.variantSetDBIds() TODO add later

                        callSet
                    }.toList()

        }

    }

    /**
     * Function to build a list of CallSets from a graph.
     * This will loop through the taxa found in each graph and make a CallSet for each on.
     */
    fun generateCallSetsFromGraph(graph: HaplotypeGraph) : List<CallSet> {
        val taxonNameToGenoIdMap = getTaxonToGenoIdMap()

        val taxaSetForGraph = graph.taxaInGraph().map { it.name }.toSet()

        return taxonNameToGenoIdMap
                .filter { taxaSetForGraph.contains(it.key) }
                .map {
                    val callSet = CallSet()
                    callSet.callSetDbId = "${it.value}"
                    callSet.callSetName = "${it.key}"
                    callSet.sampleDbId = "${it.value}"
                    // callSet.created() TODO add later
                    // callSet.updated() TODO add later
                    // callset.variantSetDBIds() TODO add later

                    callSet
                }.toList()
    }

    /**
     * Function to build call sets directly from genoids.
     *
     * This is used for both haplotype and path based callsets.
     */
    fun buildCallSetsFromIds(variantSetDbId: String,callSetIds : Set<Int>) :List<CallSet> {
        val query = "SELECT genoid, line_name " +
                "FROM genotypes " +
                "WHERE genoid IN (${callSetIds.joinToString(",")});"

        println(query)
        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->
            try {
                val outputCallSets = mutableListOf<CallSet>()

                connection.createStatement().executeQuery(query).use { rs ->
                    while (rs.next()) {
                        val genoId = rs.getInt(1)
                        val name = rs.getString(2)

                        outputCallSets.add(CallSet(callSetName = name, callSetDbId = "${genoId}", sampleDbId = "${genoId}",variantSetDbIds = listOf(variantSetDbId)))
                    }

                    return outputCallSets.toList()
                }
            } catch (e: Exception) {
                myLogger.debug(e.message, e)
                e.printStackTrace()
                throw IllegalStateException("NodeService: getAllDBIdsAndTypes: Problem querying the database: ${e.message}")
            }
        }
    }

    /**
     * Method to create a map of genoId to genotype name.
     *
     * This will only retrieve the requested genoids.
     */
    fun buildGenoIdToNameMap(callSetIds: Set<Int>) : Map<Int, String> {

        val query = "SELECT genoid, line_name " +
                "FROM genotypes " +
                "WHERE genoid IN (${callSetIds.joinToString(",")});"


        DataSource.connection.use { connection ->
            try {
                val outputMap = mutableMapOf<Int, String>()

                connection.createStatement().executeQuery(query).use { rs ->
                    while (rs.next()) {
                        val genoId = rs.getInt(1)
                        val name = rs.getString(2)

                        outputMap[genoId] = name

                    }
                    return outputMap.toMap()
                }
            } catch (e: Exception) {
                myLogger.debug(e.message, e)
                e.printStackTrace()
                throw IllegalStateException("NodeService: buildGenoIdToNameMap: Problem querying the database: ${e.message}")
            }
        }

    }

}