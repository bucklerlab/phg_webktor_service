package model

import java.util.*

data class NodeResponse(val metadata: Metadata,
                        val result: Node,
                        val _atContext: Context? = null) {
    override fun equals(o: Any?): Boolean {
        if (this === o) {
            return true
        }
        if (o == null || javaClass != o.javaClass) {
            return false
        }
        val nodeResponse = o as NodeResponse
        return this._atContext == nodeResponse._atContext &&
                metadata == nodeResponse.metadata &&
                result == nodeResponse.result
    }

    override fun hashCode(): Int {
        return Objects.hash(_atContext, metadata, result)
    }

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("class CallSetResponse {\n")
        sb.append("    _atContext: ").append(toIndentedString(_atContext)).append("\n")
        sb.append("    metadata: ").append(toIndentedString(metadata)).append("\n")
        sb.append("    result: ").append(toIndentedString(result)).append("\n")
        sb.append("}")
        return sb.toString()
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private fun toIndentedString(o: Any?): String? {
        return o?.toString()?.replace("\n", "\n    ") ?: "null"
    }
}