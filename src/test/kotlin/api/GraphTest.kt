package api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.doubles.shouldBeLessThan
import io.kotest.matchers.shouldBe
import model.*

class GraphTest : StringSpec({
    "Test /graphs from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/graphs"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        val resultAsList: GraphListResponse = mapper.readValue(result)

        println("\nGraph Result  size: ${resultAsList.result.data.size}")
        resultAsList.result.data.size shouldBe 4
    }

    "Test /graphs/CONSENSUS from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/graphs/CONSENSUS"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        val graphResponse : GraphResponse = mapper.readValue(result)

        val graph = graphResponse.result

        //check number of nodes
        val nodeList = graph.nodes
        nodeList.size shouldBe 30
        //check number of edges
        val edgeList = graph.edges

        edgeList?.size shouldBe 81

    }

    "Test /graphs/CONSENSUS/nodes from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/graphs/CONSENSUS/nodes"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        val nodeResponse : NodeListResponse = mapper.readValue(result)

        val nodeList = nodeResponse.result.data

        //check number of nodes
        nodeList.size shouldBe 30
    }
    "Test /graphs/CONSENSUS/nodes/111 from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/graphs/CONSENSUS/nodes/111"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        val nodeResponse : NodeResponse = mapper.readValue(result)

        val node = nodeResponse.result
        val additionalInfo = node.additionalInfo


        node.nodeDbId shouldBe 111

        (additionalInfo["refRangeId"] as Int) shouldBe 1
        (additionalInfo["refRangeChr"] as String) shouldBe "1"
        (additionalInfo["refRangeStart"] as Int) shouldBe 1
        (additionalInfo["refRangeEnd"] as Int) shouldBe 3000
        (additionalInfo["taxaList"] as List<*>) shouldBe listOf("Ref", "RefA1")
        (additionalInfo["seqLen"] as Int) shouldBe 2992
        (additionalInfo["asmContig"] as String) shouldBe "0"
        (additionalInfo["asmStart"] as Int) shouldBe 0
        (additionalInfo["asmEnd"] as Int) shouldBe 0
        (additionalInfo["genomeFileId"] as Int) shouldBe -1

    }


    "Test /graphs/CONSENSUS/edges from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/graphs/CONSENSUS/edges"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        val edgeResponse : EdgeListResponse = mapper.readValue(result)

        val edgeList = edgeResponse.result.data

        //check number of nodes
        edgeList.size shouldBe 81
    }

    "Test /edges from smallSeqDB Consensus Edges" {
        val query = "http://localhost:8080/brapi/v2/graphs/CONSENSUS/edges"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        val edgeResponse : EdgeListResponse = mapper.readValue(result)

        val edgeList = edgeResponse.result.data

        val firstEdge = edgeList.filter { it.edgeDbId == "111_108" }.first()
        firstEdge.leftNodeDbId shouldBe 111
        firstEdge.rightNodeDbId shouldBe 108
        (firstEdge.weight - 0.9980039920159682) shouldBeLessThan 0.001

        val secondEdge = edgeList.filter { it.edgeDbId == "111_107" }.first()
        secondEdge.leftNodeDbId shouldBe 111
        secondEdge.rightNodeDbId shouldBe 107
        (secondEdge.weight - 9.980039920159682E-4) shouldBeLessThan 0.001

        val thirdEdge = edgeList.filter { it.edgeDbId == "111_106" }.first()
        thirdEdge.leftNodeDbId shouldBe 111
        thirdEdge.rightNodeDbId shouldBe 106
        (thirdEdge.weight - 9.980039920159682E-4) shouldBeLessThan 0.001

    }

})
