package api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.kotest.core.spec.style.StringSpec
import model.VariantsListResponse

/**
 * NOTE: THis class assumes the application.conf file is setup to go to cbsudc01, db=maize_1_0
 * Or any other db that has a larger set of reference ranges than does phgsmallseq_test db.
 */
class VariantTableVariantsTest : StringSpec( {

    "Test /variantTables/HAP_PATH_METHOD/variants from smallSeqDB" {

        // TO run this test, the application.conf file should have data to point to
        // the default phgsmallseq_test db on cbsudc01.  The phg_config_file should
        // also be a config file with data to connect to phsmallseq_test
        // Using page size of 1 for small seq to verify it loops through all pages.
        val variantsQuery = "http://localhost:8080/brapi/v2/variantTables/mummer4/variants?page=0&pageSize=1"
        val variantsResult = khttp.get(variantsQuery).text
        println("\n Result from test: $variantsResult")

        val mapper = jacksonObjectMapper()

        val variantsListResult: VariantsListResponse = mapper.readValue(variantsResult)
        val variantsInfo = variantsListResult.result
        var pagination = variantsListResult.metadata.pagination
        var nextToken:String? = pagination?.nextPageToken
        var totalCount = pagination?.totalCount
        var currentPage = pagination?.currentPageToken
        println("page= ${currentPage}, nextToken=${nextToken} totalCount=${totalCount}")

        while (nextToken != null) {
            val variantsQuery = "http://localhost:8080/brapi/v2/variantTables/mummer4/variants?page=${nextToken}&pageSize=1"
            val variantsResult = khttp.get(variantsQuery).text
            //println("\n Result from test: $variantsResult")

            var variantsListResult: VariantsListResponse = mapper.readValue(variantsResult)
            pagination = variantsListResult.metadata.pagination
            totalCount = pagination?.totalCount
            currentPage = pagination?.currentPageToken
            nextToken = pagination?.nextPageToken
            println("page= ${currentPage}, nextToken= ${nextToken} totalCount=${totalCount}")

        }

        println("\n FINished!!  last page received was $currentPage")
        //variantsListResult.result.data.size shouldBe 10 // only for smallSeq !!
    }
    "Test /variantTables/v5_gffGenes/variants from maize_1_0" {

        // TO run this test, the application.conf file must be set to point to
        // the maize_1_0 database (same server, cbsudc01)
        // ANd the phg config file in the application.conf file must point to a config
        // file that will connect to the maize_1_0 instance
        // When bringing up the server, you must give it a few minutes to create the graphs.
        // It is best to comment out lines in GraphService.kt:buildGraph() that build the graphs,
        // as the path graph building is very slow.  Those are the lines for comprising this:
        //    methods.forEach { method -> ... }

        // BOth of these methods work:
        // v5_gffGenes works.  mummer4_NAM_released_assemblies works
        val variantsQuery = "http://localhost:8080/brapi/v2/variantTables/mummer4_NAM_released_assemblies/variants?page=0&pageSize=1000"

        val variantsResult = khttp.get(variantsQuery).text
        //println("\n Result from test: $variantsResult")

        val mapper = jacksonObjectMapper()

        val variantsListResult: VariantsListResponse = mapper.readValue(variantsResult)
        val variantsInfo = variantsListResult.result
        var pagination = variantsListResult.metadata.pagination
        var nextToken:String? = pagination?.nextPageToken
        var totalCount = pagination?.totalCount
        var currentPage = pagination?.currentPageToken
        println("page= ${currentPage}, nextToken=${nextToken} totalCount=${totalCount}")

        while (nextToken != null) {
            val variantsQuery = "http://localhost:8080/brapi/v2/variantTables/mummer4_NAM_released_assemblies/variants?page=${nextToken}&pageSize=1000"
            val variantsResult = khttp.get(variantsQuery).text
            //println("\n Result from test: $variantsResult")

            var variantsListResult: VariantsListResponse = mapper.readValue(variantsResult)
            pagination = variantsListResult.metadata.pagination
            totalCount = pagination?.totalCount
            currentPage = pagination?.currentPageToken
            nextToken = pagination?.nextPageToken
            println("page= ${currentPage}, nextToken - ${nextToken}, totalCount=${totalCount}")

        }

        println("\n FINished!!  last page received was $currentPage")
    }

})