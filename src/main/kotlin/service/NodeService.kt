package service

import model.Node
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import org.apache.log4j.Logger
import java.util.*

private val myLogger = Logger.getLogger(NodeService::class.java)
class NodeService {


    fun getAllNodesForAllGraphs() : List<Node> {

        val methodIds = getMethodIdsFromDB()

        return methodIds.asSequence() //Casting to sequence so its lazy
                .map { buildGraph(it) }
                .filterNotNull()
                .flatMap { getAllNodesForGraph(it) }
                .toList()
    }

    fun getAllNodesForGraph(graph : HaplotypeGraph) : List<Node> {
        return graph.referenceRanges()
                .flatMap { refRange -> graph.nodes(refRange) }
                .map{ node -> createAPINodeFromHaplotypeNode(node) }
    }

    /**
     * Function to get all the haplotype Node Ids in the Database and store them into a Sorted Set.
     * This can then be used to get a graph using all the nodes by ID.
     */
    private fun getNodeIdsFromDB() : SortedSet<Int> {
        val sqlStatement = "SELECT distinct haplotypes_id " +
                "FROM haplotypes " +
                "ORDER BY haplotypes_id;"

        myLogger.info("getNodeIdsFromDB: query statement: $sqlStatement")

        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->

            try {
                val idSet = sortedSetOf<Int>()
                connection.createStatement().executeQuery(sqlStatement).use { rs ->

                    val time = System.nanoTime()

                    while (rs.next()) {
                        idSet.add(rs.getInt(1))
                    }

                    myLogger.info("getNodeIdsFromDB: number of haplotypeIds: ${idSet.size}")
                    myLogger.info("""getNodeIdsFromDB: time: ${(System.nanoTime() - time) / 1e9} secs.""")

                    return idSet
                }
            } catch (e: Exception) {
                myLogger.debug(e.message, e)
                e.printStackTrace()
                throw IllegalStateException("NodeService: getNodeIdsFromDB: Problem querying the database: ${e.message}")
            }
        }
    }

    fun getNodeInGraph(graph: HaplotypeGraph, nodeId : Int) : Node? {
        return graph.referenceRanges().flatMap { graph.nodes(it) }
                .filter{ it.id() == nodeId }
                .map{ createAPINodeFromHaplotypeNode(it) }
                .firstOrNull()
    }

    fun getNode(nodeId : Int) : Node? {
        val graph = buildGraphByHapIds(sortedSetOf(nodeId))

        return if(graph.numberOfNodes() != 1) {
            null
        }
        else {
            val node = graph.nodeStream().findFirst().get()
            createAPINodeFromHaplotypeNode(node)
        }
    }

    fun createAPINodeFromHaplotypeNode(node: HaplotypeNode) : Node {
        val additionalInfoMap = mutableMapOf<String,Any>("refRangeId" to node.referenceRange().id(),
                                                        "refRangeChr" to node.referenceRange().chromosome().name,
                                                        "refRangeStart" to node.referenceRange().start(),
                                                        "refRangeEnd" to node.referenceRange().end(),
                                                        "taxaList" to node.taxaList().map { it.name },
                                                        "seqLen" to node.haplotypeSequence().length(),
                                                        "asmContig" to node.asmContig(),
                                                        "asmStart" to node.asmStart(),
                                                        "asmEnd" to node.asmEnd(),
                                                        "genomeFileId" to node.genomeFileID()
                                                )

        if(node.haplotypeSequence().sequence().isNotEmpty()) {
            additionalInfoMap["sequence"] = node.haplotypeSequence().sequence()
        }
        if(node.variantInfos().isPresent) {
            additionalInfoMap["variants"] = node.variantInfos().get()
        }

        return Node(node.id(), additionalInfoMap)
    }

}

