package service

import model.*
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.ReferenceRange
import org.apache.log4j.Logger
import utilities.filterGraphByRequestedRanges
import utilities.parseRangeString
import java.util.*
import kotlin.math.ceil
import kotlin.system.measureNanoTime

/**
 * Class to build Allele Matrices and any other responses needed by the endpoints
 */
object AlleleMatrixService {

    private val myLogger = Logger.getLogger(AlleleMatrixService::class.java)

    /**
     * Simple function to build a dummy AlleleMatrix for testing purposes
     */
    fun getBigDummyMatrix(): AlleleMatrix {

        val genotypes = listOf("0/0", "1/1", "0/1", "1/0")
        val finalGenotypes = mutableListOf<List<String>>()
        val rand = Random(12345)

        val numberVariants = 50000
        val numberTaxa = 5000

        val callSetDbIds = mutableListOf<String>()
        for (currentTaxon in 0 until numberTaxa) {
            callSetDbIds.add("taxon$currentTaxon")
        }

        //val variants = mutableListOf<VariantSummary>()
        val variants = mutableListOf<String>()
        val timeSpent = measureNanoTime {
            for (currentVariant in 0 until numberVariants) {

                variants.add(currentVariant.toString())

                val rowList = mutableListOf<String>()
                for (currentTaxon in 0 until numberTaxa) {
                    rowList.add(genotypes[rand.nextInt(4)])
                }
                finalGenotypes.add(rowList)
            }
        }

        println("Time Spent making big matrix: ${timeSpent / 1E9}")

        var callSetPagination = MatrixPagination(
            dimension = DIMENSION_TYPE.CALLSETS,
            page = 1,
            pageSize = numberVariants,
            totalCount = numberTaxa,
            totalPages = 1
        )
        var variantsPagination = MatrixPagination(
            dimension = DIMENSION_TYPE.VARIANTS,
            page = 1,
            pageSize = numberVariants,
            totalCount = numberVariants,
            totalPages = 1
        )

        val gtDataMatrix = DataMatrix(
            dataMatrixAbbreviation = "GT",
            dataMatrix = finalGenotypes,
            dataMatrixName = "Genotype",
            dataType = "String"
        )

        val matrix =
            AlleleMatrix(
                dataMatrices = listOf(gtDataMatrix),
                variantSetDbIds = listOf("dummyVariantSetDBId"),
                variants = variants,
                callSetDbIds = callSetDbIds,
                pagination = listOf(callSetPagination, variantsPagination)
            )

        return matrix

    }

    /**
     * Main Function to build an allele matrix from a requested variantSetDbId and given the specific pagination paramters.
     */
    fun alleleMatrix(
        variantSetDbId: String,
        variantPageSize: Int,
        variantPage: Int = 0,
        callSetPageSize: Int,
        callSetPage: Int = 0,
        rangeString: String = "",
        taxaToKeep: String = ""
    ): AlleleMatrix? {
        val graph = buildGraph(variantSetDbId) ?: return null
        return getTableFromGraph(graph, variantSetDbId, variantPageSize, variantPage, callSetPageSize, callSetPage, rangeString, taxaToKeep)
    }

    /**
     * Function to convert the graph into an AlleleMatrix
     */
    fun getTableFromGraph(
        graph: HaplotypeGraph,
        variantSetDbId: String,
        variantPageSize: Int,
        variantPage: Int = 0,
        callSetPageSize: Int,
        callSetPage: Int = 0,
        rangeString: String = "",
        taxaToKeep: String = ""
    ): AlleleMatrix {

        val totalMethodStTime = System.nanoTime()
        //paginating by referenceRange
        val rangesToKeep = if (rangeString.isEmpty()) listOf<ReferenceRange>() else parseRangeString(rangeString)

        val refRangeFilterStTime = System.nanoTime()
        val referenceRanges = filterGraphByRequestedRanges(graph.referenceRangeList().toList().sorted(), rangesToKeep)
        val refRangeFilterEndTime = System.nanoTime()


        val refRangeMapStTime = System.nanoTime()
        val variantService = VariantsService()
        val ( _ , rangeToHapIdMap) = variantService.createRefRangeMaps()
        //reverse the map
        val hapIdToIndexMap = rangeToHapIdMap.flatMap { it.value.mapIndexed { index, id -> Pair(id, index) } }.toMap()
        val refRangeMapEndTime = System.nanoTime()


        val keepTaxa = taxaToKeep
            .split(",")
            .map { it.trim() }
            .filter { it.isNotEmpty() }
            .toSortedSet()

        val chunkStTime = System.nanoTime()

        val referenceRangesChunked = referenceRanges.chunked(variantPageSize)

        val chunkEndTime = System.nanoTime()
        check(variantPage < referenceRangesChunked.size) { "Requested page is unavailable." }
        val currentSetOfRanges = referenceRangesChunked[variantPage]

        val taxonToID = getTaxonToGenoIdMap()

        val taxonStTime = System.nanoTime()
        val callSetDbIds = mutableListOf<String>()

        val allTaxon = graph.taxaInGraph().sorted()
        val totalNumTaxa = allTaxon.size
        val callsetChunks = allTaxon.chunked(callSetPageSize)
        val currentTaxaList = callsetChunks[callSetPage]

        val taxonListMap = currentTaxaList
            .filter { keepTaxa.isEmpty() || keepTaxa.contains(it.name) }
            .mapIndexed { index, taxon ->
                callSetDbIds.add(taxonToID[taxon.name].toString())
                Pair(taxon.name, index)
            }
            .toMap()

        val taxonEndTime = System.nanoTime()

        val numberOfTaxon = taxonListMap.size
        val taxonKeys = taxonListMap.keys.toHashSet()
        val outputGenotypes = mutableListOf<List<String>>()

        var totalSortTime = 0L
        var totalIndexTime = 0L
        var totalBuildListTime = 0L

        val variantDbIds = mutableListOf<String>()
        for (refRange in currentSetOfRanges) {

            variantDbIds.add(refRange.id().toString())

            //Get the nodes
            val sortStTime = System.nanoTime()
            val nodesInRange = graph.nodes(refRange).sortedBy { it.id() }
            totalSortTime += (System.nanoTime() - sortStTime)
            //Read them into taxon -> node map
            val outputCallList = (0 until numberOfTaxon).map { "./." }.toMutableList()

            //We are shifting the index for the gentoype call up by 1 in order to closely match hVCF format.
            //hVCF format is used by PHG to represent the Haplotype Matrix in a VCF like format
            //The main difference is that each SNP is a RefRange and we use symbolic alleles in the ALT column to represent individual haplotypes
            val indexStTime = System.nanoTime()
            val nodeToIndexMap =
                nodesInRange.map { Pair(it.id(),hapIdToIndexMap[it.id()]?:-2 + 1) } //Making default -2 if it cannot find the id so when we add 1 it stays -1
                    .toMap()
            totalIndexTime += (System.nanoTime() - indexStTime)

            val buildListStTime = System.nanoTime()
            for (node in nodesInRange) {
                val nodeString = "${nodeToIndexMap[node.id()]}/${nodeToIndexMap[node.id()]}"
                for (taxon in node.taxaList()) {
                    val name = taxon.name
                    if (taxonKeys.contains(name)) {
                        val taxonIndex = taxonListMap[name]
                        outputCallList[taxonIndex!!] = nodeString
                    }
                }
            }

            totalBuildListTime += (System.nanoTime() - buildListStTime)
            outputGenotypes.add(outputCallList)
        }

        var callSetPagination = MatrixPagination(
            dimension = DIMENSION_TYPE.CALLSETS,
            page = callSetPage,
            pageSize = callSetPageSize,
            totalCount = totalNumTaxa,
            totalPages = callsetChunks.size
        )
        var variantsPagination = MatrixPagination(
            dimension = DIMENSION_TYPE.VARIANTS,
            page = variantPage,
            pageSize = variantPageSize,
            totalCount = referenceRanges.size,
            totalPages = referenceRangesChunked.size
        )

        val gtDataMatrix = DataMatrix(
            dataMatrixAbbreviation = "GT",
            dataMatrix = outputGenotypes,
            dataMatrixName = "Genotype",
            dataType = "String"
        )

        val totalMethodEndTime = System.nanoTime()

        println("Total Method Time: ${(totalMethodEndTime - totalMethodStTime) / 1E9} Seconds.")
        println("Total Sort RefRangeTime: ${(refRangeFilterEndTime - refRangeFilterStTime) / 1E9} Seconds.")
        println("Total RefRange Map Time: ${(refRangeMapEndTime - refRangeMapStTime) / 1E9} Seconds.")
        println("Total Chunked Time: ${(chunkEndTime - chunkStTime) / 1E9} Seconds.")
        println("Total Taxon Filter Time: ${(taxonEndTime - taxonStTime) / 1E9} Seconds.")
        println("Total Sort Time: ${totalSortTime / 1E9} Seconds.")
        println("Total Index Time: ${totalIndexTime / 1E9} Seconds.")
        println("Total Build List Time: ${totalBuildListTime / 1E9} Seconds.")

        val matrix =
            AlleleMatrix(
                dataMatrices = listOf(gtDataMatrix),
                variantSetDbIds = listOf(variantSetDbId),
                //variants = variants,
                variants = variantDbIds,
                callSetDbIds = callSetDbIds,
                pagination = listOf(callSetPagination, variantsPagination)
            )

        return matrix
    }

}