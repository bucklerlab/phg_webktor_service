package api

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import model.*
import service.GraphService
import service.NodeService
import service.buildGraph

fun Route.graphs() {
    val graphService = GraphService
    route("/graphs") {
        get("") {
            val timeStart = System.nanoTime()
            val graphs = graphService.getAllGraphs()
            val timeEnd = System.nanoTime()

            println("Time Spent getting all graphs: ${(timeEnd - timeStart)/1e9} seconds.")
            if(graphs.isEmpty()) {
                call.respond(" ${HttpStatusCode.NotFound}: No Graphs found in the DB.")
            }
            else {
                call.respond(GraphListResponse(Metadata(), GraphListResponseResult(graphs.toTypedArray())))
            }
        }
        get("/dbIds") {
            //This will return the list of available dbIds
            val dbIds = graphService.getAllGraphDbIdModel()

            if(dbIds.isEmpty()) {
                call.respond(" ${HttpStatusCode.NotFound}: No Graphs found in the DB.")
            }
            else {
                call.respond(GraphDbIdResponse(Metadata(), GraphDbIdResponseResult(dbIds.toTypedArray())))
            }


        }
        get ( "/nodes") {
            val graphs = graphService.getAllGraphs()
            val newGraphs = graphs.map { Graph(it.graphDbId,"DirectedGraph", it.nodes) }
            if(newGraphs.isEmpty()) {
                call.respond(" ${HttpStatusCode.NotFound}: No Nodes found in the database")
            }
            else {
                call.respond(GraphListResponse(Metadata(), GraphListResponseResult(newGraphs.toTypedArray())))
            }
        }

        get("/{graphDbId}") {
            val graphDbId = call.parameters["graphDbId"] ?: throw IllegalStateException("Must provide id")
            val graphService = GraphService

            val graph = graphService.buildAPIGraphForId(graphDbId)

            if(graph == null) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested Graph object ${graphDbId} was not found in the database")
            }
            else {
                call.respond(GraphResponse(Metadata(), graph))
            }
        }
        get("/{graphDbId}/nodes") {
            val graphDbId = call.parameters["graphDbId"] ?: throw IllegalStateException("Must provide id")

            val graph = graphService.buildAPIGraphForId(graphDbId)
            if(graph == null) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested Graph object ${graphDbId} was not found in the database")
            }
            else {
                val nodes = graph.nodes
                if(nodes.isEmpty()) {
                    call.respond(" ${HttpStatusCode.NotFound}: The Nodes for the requested Graph object ${graphDbId} are not found in the database")
                }
                else {
                    call.respond(NodeListResponse(Metadata(), NodeListResponseResult(nodes.toTypedArray())))
                }
            }
        }
        get("/{graphDbId}/nodes/{nodeDbId}") {
            val graphDbId = call.parameters["graphDbId"] ?: throw IllegalStateException("Must provide id")
            val nodeDbId = call.parameters["nodeDbId"]?.toInt() ?: throw IllegalStateException("Must provide a node id")

            val graph = buildGraph(graphDbId)

            if(graph == null) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested Graph object ${graphDbId} was not found in the database")
            }
            else {
                val nodeService = NodeService()
                val currentNode = nodeService.getNodeInGraph(graph, nodeDbId)
                if(currentNode==null) {
                    call.respond(" ${HttpStatusCode.NotFound}: The Node with requested nodeDbId: ${nodeDbId} for the requested Graph object ${graphDbId} is not found in the database")
                }
                else {
                    call.respond(NodeResponse(Metadata(), currentNode))
                }
            }
        }
        get("/{graphDbId}/edges") {
            val graphDbId = call.parameters["graphDbId"] ?: throw IllegalStateException("Must provide id")

            val graph = graphService.buildAPIGraphForId(graphDbId)

            if(graph == null) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested Graph object ${graphDbId} was not found in the database")
            }
            else {
                val edges = graph.edges

                if((edges?:listOf()).isEmpty()) {
                    call.respond(" ${HttpStatusCode.NotFound}: The Edges for the requested Graph object ${graphDbId} are not found in the database")
                }
                else {
                    call.respond(EdgeListResponse(Metadata(), EdgeListResponseResult((edges?:listOf()).toTypedArray())))
                }
            }
        }
    }
}