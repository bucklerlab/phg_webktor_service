package api

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import model.Metadata
import model.ReferenceSingleResponse
import model.ReferencesListResponse
import model.ReferencesListResponseResult
import service.ReferencesService

fun Route.references() {

    val references = ReferencesService()

    route("/references") {

        get("") {
            call.respond(
                ReferencesListResponse(
                    Metadata(),
                    ReferencesListResponseResult(references.getReferences().toTypedArray())
                )
            )
        }

        get("/{referenceDbId}") {
            // Assumes user passed in a chromosome name (String)
            val id = call.parameters["referenceDbId"] ?: throw IllegalStateException("Must provide referenceDbId")
            val reference = references.getReferencesForId(id)
            // This returns an empty Reference object if there was no data.
            if (reference == null || reference.referenceName == null) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested object ${id} was not found in the database")
            } else {
                call.respond(ReferenceSingleResponse(Metadata(),reference))
            }
        }
    }
}