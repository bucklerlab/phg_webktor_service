package service

import model.Call
import model.ListValue
import model.TokenPagination
import model.Variant
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.util.Sizeof
import org.apache.log4j.Logger
import org.ehcache.config.builders.CacheConfigurationBuilder
import org.ehcache.config.builders.CacheManagerBuilder
import org.ehcache.config.builders.ResourcePoolsBuilder
import org.ehcache.core.internal.statistics.DefaultStatisticsService
import org.ehcache.core.spi.service.StatisticsService
import utilities.convertReferenceRangeToVariant

val statisticsVariantsService: StatisticsService = DefaultStatisticsService()
/**
 * Class to handle all the Variant creation.
 */

// NOTE: when using the variantCache to pull reference range data,
// use variantDbId-1.  The variantDbIds are 1-based:  actual reference range ids
// These are stored in order, in a list that is 0-based.
private val cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
    .using(statisticsVariantsService)
    .withCache(
        "variantCache",
        CacheConfigurationBuilder.newCacheConfigurationBuilder(
            String::class.java,
            ArrayList::class.java,
            ResourcePoolsBuilder.heap(1).build()
        )
    )
    .build(true)

private val variantCache = cacheManager.getCache("variantCache",String::class.java, ArrayList::class.java)

class VariantsService {

    private val myLogger = Logger.getLogger(VariantsService::class.java)

    init {
        println("VariantsService:init -  heapSIze before create variants cache:")
        Sizeof.printMemoryUse()
        val referenceRanges = createRefRangesForCache() as ArrayList<ReferenceRange>
        if (referenceRanges != null) {
            variantCache.put("all",referenceRanges)
            val ehCacheStat = statisticsVariantsService.getCacheStatistics("variantCache");
            // using println until we get myLogger working
            println("variant (reference Range) cache size: ${ehCacheStat.getTierStatistics().get("OnHeap")?.getMappings()}")
            myLogger.info("variant (reference Range) cache size: ${ehCacheStat.getTierStatistics().get("OnHeap")?.getMappings()}")
        }
        println("VariantsService:init -  heapSize after create variants cache:")
        Sizeof.printMemoryUse()
    }

    val methodIdToNameMap = createMethodIdToNameMap()

    fun createMethodIdToNameMap():Map<Int, String> {
        myLogger.info("starting to build method_id to method_name map")

        val idToNameMap = HashMap<Int, String>()

        val query = "SELECT method_id,name from methods;"

        // The connection is automatically closed when this block finishes.
        DataSource.connection.use { connection ->
            try {
                // map the method_ids to method names
                connection.createStatement().executeQuery(query).use { rs ->

                    while (rs.next()) {
                        val methodId = rs.getInt(1)
                        val name = rs.getString(2)
                        idToNameMap.put(methodId, name)
                    }
                }
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                exc.printStackTrace()
                throw IllegalStateException("VariantsService: createMethodIdToNameMap: Problem querying the database: ${exc.message}")
            }

            return idToNameMap
        }
    }

    /**
     * Function to create a refRange to methods Map
     * From the haploytpes table, find all methods associated with each reference range.
     * To make this quick, I'm doing 2 queries:
     *   1.  query methods table for id->name map
     *   2.  query haplotypes table for refRanges->methodIds.
     *      There will be multiple methodIds for each reference range.
     *   Both the method_id and the associated name are returned so the
     *   user can make use of which ever is needed.
     *
     * Will hit the database and dump the haplotypes table to get this information.
     *
     */
    fun createRefRangeIdToMethodNameMap() :Map<Int, Set<String>> {
        myLogger.info("Starting to build HapIdToRefRangeMap;")
        val query = "SELECT ref_range_id,method_id FROM haplotypes;"

        // The connection is automatically closed when this block finishes.
        DataSource.connection.use { connection ->
            try {
                val rangesToMethodNames = mutableMapOf<Int, MutableSet<String>>()

                // map the refRangeIds to a list of method_names
                connection.createStatement().executeQuery(query).use { rs ->

                    val time = System.nanoTime()

                    while (rs.next()) {
                        val refRangeId = rs.getInt(1)
                        val methodId = rs.getInt(2)
                        val methodName = methodIdToNameMap.get(methodId)!!

                        if (rangesToMethodNames.containsKey(refRangeId)) {
                            rangesToMethodNames[refRangeId]?.add(methodName)
                        } else {
                            rangesToMethodNames[refRangeId] = mutableSetOf<String>(methodName)
                        }
                    }
                    val endTime = (System.nanoTime() - time)/1e9
                    myLogger.info("createRefRangeIdToMethodNameMap: time to create map of refRangeId to Methods: ${endTime} seconds")
                    // remove println when logger is working
                    println("createRefRangeIdToMethodNameMap: time to create map of refRangeId to Methods: ${endTime} seconds")

                    return rangesToMethodNames
                }
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                exc.printStackTrace()
                throw IllegalStateException("VariantsService: createRefRangeIdToMethodNameMap: Problem querying the database: ${exc.message}")
            }
        }
    }

    fun createRefRangeMaps() :Pair<Map<Int, Set<String>>, Map<Int,List<Int>>> {
        myLogger.info("Starting to build HapIdToRefRangeMap;")
        val query = "SELECT ref_range_id,method_id, haplotypes_id FROM haplotypes;"

        // The connection is automatically closed when this block finishes.
        DataSource.connection.use { connection ->
            try {
                val rangesToMethodNames = mutableMapOf<Int, MutableSet<String>>()
                val rangesToHapIds = mutableMapOf<Int,MutableList<Int>>()

                // map the refRangeIds to a list of method_names
                connection.createStatement().executeQuery(query).use { rs ->

                    val time = System.nanoTime()

                    while (rs.next()) {
                        val refRangeId = rs.getInt(1)
                        val methodId = rs.getInt(2)
                        val methodName = methodIdToNameMap.get(methodId)!!

                        val haplotypeId = rs.getInt(3)

                        if (rangesToMethodNames.containsKey(refRangeId)) {
                            rangesToMethodNames[refRangeId]?.add(methodName)
                        } else {
                            rangesToMethodNames[refRangeId] = mutableSetOf<String>(methodName)
                        }

                        if (rangesToHapIds.containsKey(refRangeId)) {
                            rangesToHapIds[refRangeId]?.add(haplotypeId)
                        } else {
                            rangesToHapIds[refRangeId] = mutableListOf<Int>(haplotypeId)
                        }
                    }
                    val endTime = (System.nanoTime() - time)/1e9
                    myLogger.info("createRefRangeIdToMethodNameMap: time to create map of refRangeId to Methods: ${endTime} seconds")
                    // remove println when logger is working
                    println("createRefRangeIdToMethodNameMap: time to create map of refRangeId to Methods: ${endTime} seconds")

                    return Pair(rangesToMethodNames, rangesToHapIds)
                }
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                exc.printStackTrace()
                throw IllegalStateException("VariantsService: createRefRangeIdToMethodNameMap: Problem querying the database: ${exc.message}")
            }
        }
    }


    // Method creates an ordered (by reference range id) list of PHG ReferenceRange objects
    fun createRefRangesForCache(): List<ReferenceRange> {
        val query = "SELECT ref_range_id, chrom, range_start, range_end from reference_ranges ORDER BY ref_range_id;"
        val refRangeList = mutableListOf<ReferenceRange>()
        DataSource.connection.use { connection ->
            try {
                connection.createStatement().executeQuery(query).use { rs ->

                    while (rs.next()) {
                        val refRangeId = rs.getInt("ref_range_id")
                        val chrom = rs.getString("chrom")
                        val rangeStart = rs.getInt("range_start")
                        val rangeEnd = rs.getInt("range_end")

                        // Variant names must be  consistent with the 2 convertReferenceRange... methods above in terms
                        // of how the name is created.
                        val referenceRange =
                            ReferenceRange("ref", Chromosome.instance(chrom), rangeStart, rangeEnd, refRangeId);
                        refRangeList.add(referenceRange)
                    }
                }
                return refRangeList
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                exc.printStackTrace()
                throw IllegalStateException("VariantsService: createRefRangesForCache: Problem querying the database: ${exc.message}")
            }
        }
    }

    /**
     * Function to build a list of db Variants.
     * The variants are the reference ranges.  Each variant object includes a list of variantSetDbIds
     * which include this variant.
     * The variantSets for PHG are methods.  The variantSetDbIds for each reference range is the
     * set of PHG method names that, for each reference range, identify all the haplotypes (based on
     * method_id) stored with this reference range id.
     *
     * This must be paginated.  This is a list ordered by
     * ref range id, such that token pagination will get the variant based on the id and next
     * one is pageSize+start.  If start =0, size=10, next= start+size = 10, which is correct
     */
    fun generateVariantsListFromCache(currentPageToken: Int, pageSize: Int, rangeGroup: String): Pair<TokenPagination,List<Variant>> {

        //check for cache - if it doesn't exist, create it
        // remove println when myLogger is working
        var referenceRanges: ArrayList<ReferenceRange>? = null
        if (variantCache.containsKey(rangeGroup)) {
            myLogger.info("generateVariantsListFromCache - getting reference/variants from cache")
            println("generateVariantsListFromCache - getting reference/variants from cache")
            referenceRanges = variantCache[rangeGroup] as ArrayList<ReferenceRange>
        } else {
            myLogger.info("generateVariantsListFromCache - no variantCache - creating it, heapSIze before create cache:")
            println("generateVariantsListFromCache - no variantCache - creating it, heapSIze before create cache:")
            Sizeof.printMemoryUse()
            referenceRanges = createRefRangesForCache() as ArrayList<ReferenceRange>
            variantCache.put(rangeGroup,referenceRanges)
            myLogger.info("generateVariantsListFromCache: heapSize after create variant cache:")
            println("generateVariantsListFromCache: heapSize after create variant cache:")
            Sizeof.printMemoryUse()
        }

        // query db for all methods associated with each reference range
        var time = System.nanoTime()
        val (refRangeIdToMethodNameMap, refRangeToHapIdMap) = createRefRangeMaps()
        val refRangeList = refRangeIdToMethodNameMap.keys.sorted()
        var executeTime = (System.nanoTime() - time)/1e9

        if (referenceRanges == null || referenceRanges.size < 1) {
            return Pair(TokenPagination(pageSize = pageSize, nextPageToken = null, currentPageToken = currentPageToken.toString(), totalCount = 0), emptyList())
        }
        // This calculation rounds up
        val totalPages = (referenceRanges.size + pageSize - 1)/pageSize
        if (currentPageToken >= referenceRanges.size) {
            return Pair(TokenPagination(pageSize = pageSize, nextPageToken = null, currentPageToken = currentPageToken.toString(), totalCount = totalPages), emptyList())
        }

        val startId = currentPageToken-1 // because tokens are 1 based, but list is 0-based
        val endId = startId + pageSize -1; // endId is inclusive
        val totalCount = referenceRanges.size

        time = System.nanoTime()
        val filteredRanges = referenceRanges.filter{it.id() in startId..endId}
        executeTime = (System.nanoTime() - time)/1e9


        time = System.nanoTime()
        val allVariantsList = filteredRanges.map{ refRange ->
            convertReferenceRangeToVariantWithMethodList(refRange,refRangeIdToMethodNameMap.get(refRange.id())!!.toList(),refRangeToHapIdMap[refRange.id()]?.sorted()?.map { "$it" }?: emptyList())
        }
        executeTime = (System.nanoTime() - time)/1e9

        // Plus 2:  Because we dropped 1 to go to 0-based for the referenceRanges list.
        // Now we need to add that back to get to 1-based, plus add 1 more to get to the
        // first variant beyond what we've already processed.
        var nextPageToken: String? = (endId+2).toString()

        if (endId  >= refRangeList.size) {
            // no nextPageToken
            nextPageToken = null
        }

        var pagination = TokenPagination(pageSize=pageSize, nextPageToken=nextPageToken, currentPageToken=currentPageToken.toString(), totalCount=totalCount, totalPages=totalPages)
        return Pair<TokenPagination, List<Variant>>(pagination,allVariantsList)
    }

    /**
     * Function to get the specifics of a single variant (ie PHG reference range)
     * There is no paging here as a single variant is returned.
     */
    fun generateVariantFromID(variantDbId: Int, pageNumber: Int, pageSize: Int,rangeGroup:String): Variant? {
        var referenceRanges: ArrayList<ReferenceRange>? = null
        // remove println when myLogger is working
        if (variantCache.containsKey(rangeGroup)) {
            myLogger.info("generateVariantFromID: getting reference/variants from cache")
            println("generateVariantFromID: getting reference/variants from cache")
            referenceRanges = variantCache[rangeGroup] as ArrayList<ReferenceRange>
        } else {
            myLogger.info("generateVariantFromID: no variantCache - creating it, pulling references - here is heap size before cache create:")
            println("generateVariantFromID: no variantCache - creating it, pulling references - here is heap size before cache create:")
            Sizeof.printMemoryUse()
            referenceRanges = createRefRangesForCache() as ArrayList<ReferenceRange>
            variantCache.put(rangeGroup,referenceRanges)
            myLogger.info("generateVariantFromID: heapSize after create variant cache:")
            println("generateVariantFromID: heapSize after create variant cache:")
            Sizeof.printMemoryUse()
        }
        // query db for all methods associated with each reference range
        val (refRangeIdToMethodNameMap, refRangeToHapIdMap) = createRefRangeMaps()

        val range = referenceRanges.get(variantDbId-1) // variantDbId is reference range, cache is ordered by refRange but is 0-based
        return Variant(end = range.end(), referenceBases = "", referenceName = range.chromosome().name, start = range.start(),
            svlen = range.end() - range.start() +1, variantDbId = "${variantDbId}",
            variantNames = listOf("RefRange_${variantDbId}"), variantSetDbId = refRangeIdToMethodNameMap.get(variantDbId)!!.toList(),
            variantType = "REF_RANGE", alternateBases = refRangeToHapIdMap[variantDbId]?.sorted()?.map { "$it" }?: emptyList())
    }

    // This is paginated based on genoid (ie callDbId)
    fun generateCallsForVariant(variantDbId: Int, pageToken: Int, pageSize: Int): Pair<List<Call>,Int?> {

        val methodTypes = listOf(DBLoadingUtils.MethodType.ANCHOR_HAPLOTYPES.value, DBLoadingUtils.MethodType.ASSEMBLY_HAPLOTYPES.value)
        val methodIds = getMethodIdsForMethodType(methodTypes)

        val callListAndNextToken = createCallListForVariant(variantDbId,methodIds,pageToken,pageSize)
        return callListAndNextToken

    }

    /**
     * Function returns a list of method_ids from the PHG methods table
     * where the method_type falls within the supplied list
     */
    fun getMethodIdsForMethodType(methodTypes:List<Int>): List<Int> {
        val methodIds = mutableListOf<Int>()
        if (methodTypes.size == 0) {
            myLogger.warn("getMethodIdsForMethodType: empty methodTypes list")
            println("getMethodIdsForMethodType: empty methodTypes list")
            return methodIds
        }

        val query = "SELECT method_id from methods where method_type IN (${methodTypes.joinToString(",")});"

        DataSource.connection.use { connection ->
            try {

                connection.createStatement().executeQuery(query).use { rs ->
                    while (rs.next()) {
                        val methodId = rs.getInt(1)
                        methodIds.add(methodId)
                    }
                }
                return methodIds
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                exc.printStackTrace()
                throw IllegalStateException("VariantsService: getMethodIdsForMethodType: Problem querying the database: ${exc.message}")
            }
        }
    }

    /**
     * Function to create a call list for a specific variant (reference range id)
     *
     * It returns a Pair consisting of the CallList and an integer representing the nextPageToken
     *
     * This is paginated based on genoid, which becomes the callSetDbId.
     * Because not all genoids will have haplotypes at a given reference range, it returns
     * calls based on the first genoid that is equal or greater than the passed "start".
     * It returns as "nextPageToken" the next genoid in the list after we've processed
     * the specified "pageSize".
     */
    fun createCallListForVariant(variantDbId:Int, methodIds:List<Int>, start:Int, pageSize:Int): Pair<List<Call>,Int?> {
        val methodIdString = methodIds.joinToString(",")
        val query = "SELECT  haplotypes.haplotypes_id, gametes.genoid " +
                "FROM haplotypes " +
                "INNER JOIN methods ON haplotypes.method_id = methods.method_id " +
                "INNER JOIN gamete_haplotypes ON haplotypes.gamete_grp_id = gamete_haplotypes.gamete_grp_id " +
                "INNER JOIN gametes ON gamete_haplotypes.gameteid = gametes.gameteid " +
                "WHERE haplotypes.method_id IN (${methodIdString}) and haplotypes.ref_range_id=${variantDbId}"

        myLogger.info("createCallListForVariant: query statement: $query")
        println("createCallListForVariant: query statement: $query")

        val genoidToHaplotypes = mutableMapOf<Int, MutableList<Int>>()
        val genoidSet = mutableSetOf<Int>()
        DataSource.connection.use { connection ->
            try {

                connection.createStatement().executeQuery(query).use { rs ->
                    while (rs.next()) {
                        val haplotypesId = rs.getInt(1)
                        val genoid = rs.getInt(2)

                        genoidSet.add(genoid)
                        if (genoidToHaplotypes.containsKey(genoid)) {
                            genoidToHaplotypes[genoid]?.add(haplotypesId!!)
                        } else {
                            genoidToHaplotypes[genoid] = mutableListOf<Int>(haplotypesId!!)
                        }
                    }

                    // Get the mapping from genoid to taxon name
                    val callSetsService = CallSetsService()
                    val genoidToTaxonMap = callSetsService.buildGenoIdToNameMap(genoidSet)

                    // Create the call objects for the list
                    val callList = mutableListOf<Call>()
                    val sortedGenoids = genoidSet.sorted()
                    var nextGenoid: Int? = null
                    var count=0
                    for (genoid in sortedGenoids) {
                        if (genoid < start) continue
                        count++
                        if (count > pageSize) {
                            nextGenoid = genoid
                            break
                        }
                        val haplotypes = genoidToHaplotypes[genoid]!!.toList()
                        val listValue = ListValue()

                        for(haplotype in haplotypes) {
                            listValue.addValuesItem(haplotype.toString())
                        }
                        val call = Call(callSetDbId = "${genoid}",callSetName = genoidToTaxonMap[genoid]?:"",
                            genotype=listValue, variantDbId="${variantDbId}", variantName = "RefRange_${variantDbId}" )
                        callList.add(call)
                    }

                    return Pair<List<Call>,Int?>(callList,nextGenoid)
                }
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                exc.printStackTrace()
                throw IllegalStateException("VariantsService: createPageOfVariants: Problem querying the database: ${exc.message}")
            }
        }

    }

    /**
     * query the db for reference ranges that fall within a specified range
     * This query will NOT return methods in the ReferenceRange method field.
     * When needed for creating the variants, they are supplied from a
     * reference_range to methods map created in a calling method.
     */
    fun createPageOfVariants(refRangeStart: Int, refRangeEnd: Int): Map<Int,ReferenceRange>? {

        val refRangeMap = mutableMapOf<Int, ReferenceRange>()

        val querySB = StringBuilder()
        querySB.append("SELECT ref_range_id, chrom, range_start, range_end ")
        querySB.append("FROM reference_ranges WHERE ref_range_id between ")
        querySB.append(refRangeStart).append(" and ").append(refRangeEnd)
        querySB.append(" ORDER BY ref_range_id")
        val query = querySB.toString()

        myLogger.info("createPageOfVariants: query statement: $query")
        DataSource.connection.use { connection ->
            try {

                connection.createStatement().executeQuery(query).use { rs ->
                    while (rs.next()) {
                        val refRangeId = rs.getInt(1)
                        val chrom = rs.getString(2)
                        val rangeStart = rs.getInt(3)
                        val rangeEnd = rs.getInt(4)

                        val referenceRange = ReferenceRange("ref", Chromosome.instance(chrom), rangeStart, rangeEnd, refRangeId);
                        refRangeMap.put(refRangeId,referenceRange)
                    }
                }
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                exc.printStackTrace()
                throw IllegalStateException("VariantsService: createPageOfVariants: Problem querying the database: ${exc.message}")
            }
        }
        return refRangeMap
    }

    /**
     * Function to build a Variant record for each reference range.
     */
    fun buildRefRangeVariantsFromGraph(graph: HaplotypeGraph, variantSetDbId: String) : List<Variant> {
        return graph.referenceRangeList()
                .map { convertReferenceRangeToVariant(it, variantSetDbId) }
    }

    /**
     * Function to build a Variant record for a single reference range.  A list of VariantSetDbIds (ie method_names from methods table)
     * is passed to this function to be stored as part of the Variant object.
     */
    fun convertReferenceRangeToVariantWithMethodList(refRange: ReferenceRange, variantSetDbIds: List<String>, alternateBases : List<String> = emptyList()) : Variant {
        return Variant(end = refRange.end(), referenceBases = "", referenceName = refRange.chromosome().name, start = refRange.start(),
                svlen = refRange.end() - refRange.start() + 1, variantDbId = "${refRange.id()}",
                variantNames = listOf("RefRange_${refRange.id()}}"), variantSetDbId = variantSetDbIds,
                alternateBases = alternateBases,
                variantType = "REF_RANGE")
    }

    /**
     * Function to build a list of Variant objects from a set of refRangeIds.
     *
     * We do this to only extract out the needed reference ranges.
     * We could probably pull down all the reference ranges, but for now we just pull them as needed.
     *
     * The output for this will return the JSON ready Variant classes.
     */
    fun buildRefRangeVariantsFromIds(variantSetDbId: String, refRangeIds: Set<Int>) : List<Variant> {
        val query = "SELECT ref_range_id, range_start, range_end, chrom " +
                "FROM reference_ranges " +
                "WHERE ref_range_id IN (${refRangeIds.joinToString(",")});"

        println(query)
        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->
            try {
                val outputVariants = mutableListOf<Variant>()

                connection.createStatement().executeQuery(query).use { rs ->

                    while (rs.next()) {
                        val refRangeId = rs.getInt(1)
                        val rangeStart = rs.getInt(2)
                        val rangeEnd = rs.getInt(3)
                        val chrom = rs.getString(4)

                        // Should be consistent with the 2 convertReferenceRange... methods above in terms
                        // of how the name is created.
                        outputVariants.add(Variant(end = rangeEnd, referenceBases = "", referenceName = chrom, start = rangeStart,
                                svlen = rangeEnd - rangeStart, variantDbId = "${refRangeId}",
                                variantNames = listOf("RefRange_${refRangeId}"), variantSetDbId = listOf(variantSetDbId),
                                variantType = "REF_RANGE"))

                    }

                    return outputVariants.toList()
                }
            } catch (e: Exception) {
                myLogger.debug(e.message, e)
                e.printStackTrace()
                throw IllegalStateException("NodeService: getAllDBIdsAndTypes: Problem querying the database: ${e.message}")
            }
        }
    }
}