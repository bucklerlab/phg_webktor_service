package api

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import model.CallsListResponse
import model.CallsListResponseResult
import model.MetadataTokenPagination
import service.CallsService

fun Route.calls() {

    val callsService = CallsService()
    route("/calls") {

        get("") {
            call.respond(CallsListResponse(MetadataTokenPagination(),CallsListResponseResult( callsService.buildCallListFromGraph().toTypedArray())))
        }

        get("/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalStateException("Must provide id")
//            val widget = widgetService.getWidget(id)
//            if (widget == null) call.respond(HttpStatusCode.NotFound)
//            else call.respond(widget)
        }
    }
}