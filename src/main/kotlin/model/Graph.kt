package model

import java.util.*

class Graph(val graphDbId: String, val type: String, val nodes: List<Node>, val edges: List<Edge>? = null) {
    override fun equals(o: Any?): Boolean {
        if (this === o) {
            return true
        }
        if (o == null || o !is Graph) {
            return false
        }
        val graph: Graph = o as Graph
        return graphDbId == graph.graphDbId &&
                nodes == graph.nodes &&
                edges == graph.edges
    }

    override fun hashCode(): Int {
        return Objects.hash(graphDbId, type, nodes, edges)
    }

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("class Graph {\n")
        sb.append("    graphDbId: ").append(toIndentedString(graphDbId)).append("\n")
        sb.append("    type: ").append(toIndentedString(type)).append("\n")
        sb.append("    nodes: ").append(toIndentedString(nodes)).append("\n")
        sb.append("    edges: ").append(toIndentedString(edges)).append("\n") //This one might need work
        sb.append("}")
        return sb.toString()
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private fun toIndentedString(o: Any?): String? {
        return o?.toString()?.replace("\n", "\n    ") ?: "null"
    }
}