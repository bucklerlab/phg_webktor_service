package api

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import model.Metadata
import model.StudyListResponse
import model.StudyListResponseResult
import model.StudySingleResponse
import service.StudiesService

fun Route.studies() {

    val studiesServices = StudiesService()

    route("/studies") {

        get("") {
            call.respond(
                StudyListResponse(
                    Metadata(),
                    StudyListResponseResult(studiesServices.allMethodNames().toTypedArray())
                )
            )
        }

        get("/{studyDbId}") {
            val id = call.parameters["studyDbId"]?.toInt() ?: throw IllegalStateException("Must provide studyDbId")
            val study = studiesServices.getMethodDataForId(id)
            if (study == null) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested object ${id} was not found in the database")
            } else {
                call.respond(StudySingleResponse(Metadata(),study))
            }
        }
    }
}