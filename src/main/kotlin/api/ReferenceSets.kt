package api

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import model.Metadata
import model.ReferenceSetsListResponse
import model.ReferenceSetsListResponseResult
import model.ReferenceSetsSingleResponse
import service.ReferenceSetsService

fun Route.referenceSets() {

    val referenceSets = ReferenceSetsService()

    route("/referencesets") {

        get("") {
            call.respond(
                ReferenceSetsListResponse(
                    Metadata(),
                    ReferenceSetsListResponseResult(referenceSets.getReferenceGenomeData().toTypedArray())
                )
            )
        }

        get("/{referenceSetDbId}") {
            // Using "id" as String - user should know their reference or assembly line name,
            // but won't know the genoid.  This allows user to query for data on either the
            // PHG reference or one of the reference assemblies
            val id = call.parameters["referenceSetDbId"]?: throw IllegalStateException("Must provide referenceSetDbId")
            val referenceSet = referenceSets.getGenomeDataForId(id)
            // This returns an empty ReferenceSet if there was no data.
            if (referenceSet == null || referenceSet.referenceSetDbId == null) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested object ${id} was not found in the database")
            } else {
                call.respond(ReferenceSetsSingleResponse(Metadata(),referenceSet))
            }
        }
    }
}