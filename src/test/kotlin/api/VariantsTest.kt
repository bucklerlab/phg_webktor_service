package api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import junit.framework.Assert.assertTrue
import model.CallsListResponse
import model.VariantSingleResponse
import model.VariantsListResponse
import service.VariantsService

class VariantsTest : StringSpec({
    "Test Variants from smallSeqDB with default page" {
        // testing variants from cbsudc01 smallseq_postgres
        val query = "http://localhost:8080/brapi/v2/variants"
        val result = khttp.get(query).text
        println("\n Result from variants test: $result")

        val mapper = jacksonObjectMapper()

        val variantsInfoResult : VariantsListResponse = mapper.readValue(result)
        val variantsInfo = variantsInfoResult.result

        val endpoints = variantsInfo.data
        //check number of nodes
        endpoints.size shouldBe 10

        val pagination = variantsInfoResult.metadata.pagination
        val nextToken:String? = pagination?.nextPageToken
        nextToken shouldBe null
    }
    "Test Variants By Id from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/variants/7"
        val result = khttp.get(query).text
        println("\n Result from test: $result")

        val mapper = jacksonObjectMapper()

        println("now get variantResult")
        val variantResult: VariantSingleResponse = mapper.readValue(result)
        println("set variantInfo to variantResult.result")
        val variantInfo = variantResult.result

        val variantName = variantInfo.variantNames
        assertTrue(variantName.contains("RefRange_7"))

        val pagination = variantResult.metadata.pagination
        val nextToken:String? = pagination?.nextPageToken
        nextToken shouldBe null

    }
    "Test Variants Calls for Id 7 from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/variants/7/calls?page=1&pageSize=3"
        val result = khttp.get(query).text
        println("\n Result from test: $result")

        val mapper = jacksonObjectMapper()

        println(" get variant calls Result")
        val variantCallsResult: CallsListResponse = mapper.readValue(result)
        println("set variantInfo to variantResult.result")
        val callsInfo = variantCallsResult.result

        // should get the nextPage token from the pagination
        val pagination = variantCallsResult.metadata.pagination
        val nextToken:String? = pagination?.nextPageToken

        nextToken shouldBe "4"
        if (nextToken != null) {
            val query = "http://localhost:8080/brapi/v2/variants/7/calls?page=${nextToken}&pageSize=3"
            val result = khttp.get(query).text
            println("\n Result of get nextToken: $result")
            val variantCallsResult: CallsListResponse = mapper.readValue(result)
            val pagination = variantCallsResult.metadata.pagination
            val nextToken:String? = pagination?.nextPageToken
            nextToken shouldBe "7"
        }
    }
    "Test Variants from smallSeqDB with pageSize 3" {
        // SmallSeq has 10 reference ranges.  With page size=3, this should
        // be 3 pages
        val query = "http://localhost:8080/brapi/v2/variants?pageSize=3"
        val result = khttp.get(query).text
        println("\n Result from variants test: $result")

        val mapper = jacksonObjectMapper()

        val variantsInfoResult : VariantsListResponse = mapper.readValue(result)
        val variantsInfo = variantsInfoResult.result

        val endpoints = variantsInfo.data
        //check number of nodes
        endpoints.size shouldBe 3

        val pagination = variantsInfoResult.metadata.pagination
        val nextPageToken = pagination!!.nextPageToken
        nextPageToken shouldBe "4"
    }
    "Test Variants from smallSeqDB with pageSize 5" {
        // SmallSeq has 10 reference ranges, so 5 gives exactly 2 pages
        val query = "http://localhost:8080/brapi/v2/variants?pageSize=5"
        val result = khttp.get(query).text
        println("\n Result from variants test: $result")

        val mapper = jacksonObjectMapper()

        val variantsInfoResult : VariantsListResponse = mapper.readValue(result)
        val variantsInfo = variantsInfoResult.result

        val endpoints = variantsInfo.data
        //check number of nodes
        endpoints.size shouldBe 5

        // Verify pagination
        val pagination = variantsInfoResult.metadata.pagination
        val nextPageToken = pagination!!.nextPageToken
        nextPageToken shouldBe "6"
    }
    "Test Variants from smallSeqDB with pageSize 11" {
        // SmallSeq has 10 reference ranges, so result should be 1 page with null for nextPage
        val query = "http://localhost:8080/brapi/v2/variants?pageSize=11"
        val result = khttp.get(query).text
        println("\n Result from variants test: $result")

        val mapper = jacksonObjectMapper()

        val variantsInfoResult : VariantsListResponse = mapper.readValue(result)
        val variantsInfo = variantsInfoResult.result

        val endpoints = variantsInfo.data
        //check number of nodes
        endpoints.size shouldBe 10

        // Verify pagination
        val pagination = variantsInfoResult.metadata.pagination
        val nextPageToken = pagination!!.nextPageToken
        nextPageToken shouldBe null
    }
    "Test Variants from smallSeqDB for page 2 of pageSize 3" {
        // Result should be 3 data entries for variants 4,5,6 with
        // nextPageToken=7
        val query = "http://localhost:8080/brapi/v2/variants?page=4&pageSize=3"
        val result = khttp.get(query).text
        println("\n Result from variants test: $result")

        val mapper = jacksonObjectMapper()

        val variantsInfoResult: VariantsListResponse = mapper.readValue(result)
        val variantsInfo = variantsInfoResult.result

        val endpoints = variantsInfo.data
        //check number of nodes
        endpoints.size shouldBe 3

        // Verify pagination
        val pagination = variantsInfoResult.metadata.pagination
        val nextPageToken = pagination!!.nextPageToken
        nextPageToken shouldBe "7"
    }
    "Test variants from maize_1_0 with default page size 1000" {
        val time = System.nanoTime()
        println("LCJ - begin test maize_1_0")
        val query = "http://localhost:8080/brapi/v2/variants?pageSize=1000"
        val result = khttp.get(query).text
        println("\n Result from variants test: $result")

        val mapper = jacksonObjectMapper()

        val variantsListResult: VariantsListResponse = mapper.readValue(result)
        val variantsInfo = variantsListResult.result
        var pagination = variantsListResult.metadata.pagination
        var nextToken:String? = pagination?.nextPageToken
        var totalCount = pagination?.totalCount
        var currentPage = pagination?.currentPageToken
        println("page= ${currentPage}, nextToken=${nextToken} totalCount=${totalCount}")
        while (nextToken != null) {
            val variantsQuery = "http://localhost:8080/brapi/v2/variants?page=${nextToken}&pageSize=1000"
            val variantsResult = khttp.get(variantsQuery).text
            //println("\n Result from test: $variantsResult")

            var variantsListResult: VariantsListResponse = mapper.readValue(variantsResult)
            pagination = variantsListResult.metadata.pagination
            totalCount = pagination?.totalCount
            currentPage = pagination?.currentPageToken
            nextToken = pagination?.nextPageToken
            println("page= ${currentPage}, nextToken - ${nextToken}, totalCount=${totalCount}")

        }

        val finalTime = (System.nanoTime() - time)/1e9
        println("\n FINished!!  last page received was $currentPage, time to execute: $finalTime seconds")
    }

    "Lynn test createRefRangeIdToMethodNameMap()" {
        // This test created to verify the results of createRefRangeIdToMethodNameMap()
        // the expected results vary based on what is in the db.  It is best tested when
        // connected to maize_1_0 as that db will have many more methods associated with
        // each  reference range.  I verify by checking the results against a db query of:
        //     select distinct(method_id) from haplotypes where ref_range_id=7;
        val variantService = VariantsService()
        val refRangeIdToMethodNameMap = variantService.createRefRangeIdToMethodNameMap()
        val methodList = refRangeIdToMethodNameMap.get(7)!!.toList()
        println("\nJUnit: Here are the methods for ref range 7:")
        for (method in methodList) {
            println(method)
        }

    }
    })