package api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.doubles.shouldBeLessThan
import io.kotest.matchers.shouldBe
import model.EdgeListResponse

class EdgeTest : StringSpec({

    "Test /edges from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/edges"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        val edgeResponse : EdgeListResponse = mapper.readValue(result)

        val edgeList = edgeResponse.result.data

        //check number of nodes
        edgeList.size shouldBe 450
    }

    "Test /edges from smallSeqDB Consensus Edges" {
        val query = "http://localhost:8080/brapi/v2/edges"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        val edgeResponse : EdgeListResponse = mapper.readValue(result)

        val edgeList = edgeResponse.result.data

        val firstEdge = edgeList.filter { it.edgeDbId == "111_108" }.first()
        firstEdge.leftNodeDbId shouldBe 111
        firstEdge.rightNodeDbId shouldBe 108
        (firstEdge.weight - 0.9980039920159682) shouldBeLessThan 0.001

        val secondEdge = edgeList.filter { it.edgeDbId == "111_107" }.first()
        secondEdge.leftNodeDbId shouldBe 111
        secondEdge.rightNodeDbId shouldBe 107
        (secondEdge.weight - 9.980039920159682E-4) shouldBeLessThan 0.001

        val thirdEdge = edgeList.filter { it.edgeDbId == "111_106" }.first()
        thirdEdge.leftNodeDbId shouldBe 111
        thirdEdge.rightNodeDbId shouldBe 106
        (thirdEdge.weight - 9.980039920159682E-4) shouldBeLessThan 0.001

    }

})
