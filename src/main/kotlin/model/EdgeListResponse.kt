package model

import kotlinx.serialization.Serializable

@Serializable
data class EdgeListResponse(
    val metadata: Metadata,
    val result: EdgeListResponseResult,
    var Atcontext: Context? = null
)

@Serializable
data class EdgeListResponseResult(val data: Array<Edge>)