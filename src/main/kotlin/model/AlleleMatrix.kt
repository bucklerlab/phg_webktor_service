package model

import kotlinx.serialization.Serializable

/**
 * Class to hold the AlleleMatrix JSON response
 */
@Serializable
data class AlleleMatrix(
    val callSetDbIds: List<String>,
    val variantSetDbIds: List<String>,
    //val variants: List<VariantSummary>,
    val variants: List<String>,
    // GT data is a GenotypeField
    // Each row of data (outer array) corresponds to a variant definition,
    // and each column (inner array) corresponds to a callSet.
    val dataMatrices: List<DataMatrix>? = emptyList(),
    val expandHomozygotes: Boolean = false,
    val pagination: List<MatrixPagination>,
    val sepPhased: String = "|",
    val sepUnphased: String = "/",
    val unknownString: String = "."
)

/**
 * Class to hold the nested DataMatrix informatino
 */
@Serializable
data class DataMatrix(
    val dataMatrixAbbreviation: String,
    val dataMatrix: List<List<String>>,
    val dataMatrixName: String,
    val dataType: String
)

enum class DIMENSION_TYPE { CALLSETS, VARIANTS }

/**
 * Class to hold the Pagination for a Matrix
 */
@Serializable
data class MatrixPagination(
    val dimension: DIMENSION_TYPE,
    val page: Int,
    val pageSize: Int,
    val totalCount: Int,
    val totalPages: Int
)

/**
 * Class to hold the Pagination and associated metadata.
 */
@Serializable
data class MetadataMatrixPagination(
    val pagination: MatrixPagination? = null,
    val datafiles: Array<DataFile>? = null,
    val status: Array<Status>? = null
)