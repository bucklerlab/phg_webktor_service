package api

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import model.*
import service.SamplesService
import service.VariantTablesService
import service.variantTablesMetaData

/**
 *  The variantTableDbId  represents a method name from the PHG methods table.
 */
fun Route.variantTables() {

    val variantTablesService = VariantTablesService()

    route("/variantTables") {

        get("") {
            call.respond(
                VariantTableMetaDataResponse(
                    Metadata(),
                    VariantTableMetaDataResponseResult(variantTablesMetaData())
                )
            )
        }
        get("/{variantTableDbId}") {
            val variantSetDbId =
                call.parameters["variantTableDbId"] ?: throw IllegalStateException("Must provide variantTableDbId")
            val result = variantTablesMetaData(variantSetDbId)
            if (result == null) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested object $variantSetDbId was not found in the database")
            } else {
                call.respond(
                    VariantTableMetaDataResponse(
                        Metadata(),
                        VariantTableMetaDataResponseResult(arrayOf(variantTablesMetaData(variantSetDbId)))
                    )
                )
            }
        }
        get("/{variantTableDbId}/table") {
            val variantSetDbId =
                call.parameters["variantTableDbId"] ?: throw IllegalStateException("Must provide variantTableDbId")

            val pageSize = call.parameters["pageSize"]?.toInt() ?: 10
            val currentPage = call.parameters["page"]?.toInt() ?: 0

            val ranges = call.parameters["ranges"] ?: ""
            val sampleNames = call.parameters["sampleNames"] ?: ""

            // Test to make dummy data
            var pagination = TokenPagination(
                pageSize = pageSize, nextPageToken = "0", currentPageToken = "$currentPage",
                totalCount = 1, totalPages = 1, prevPageToken = "0"
            )
            val currentResponse = VariantTableResponse(
                MetadataTokenPagination(pagination = pagination),
                variantTablesService.getBigDummyTable()
            )
            call.respond(currentResponse)

            val paginationAndTable =
                variantTablesService.buildGraphAndGetTable(variantSetDbId, pageSize, currentPage, ranges, sampleNames)

            //val pagination = paginationAndTable.first
            val table = paginationAndTable.second

            if (table == null) {
                call.respond(" ${HttpStatusCode.NotFound}: The requested VariantTable object ${variantSetDbId} was not found in the database")
            } else {
                //val currentResponse = VariantTableResponse(MetadataTokenPagination(pagination = pagination), table!!)

                call.respond(currentResponse)
            }
        }
        get("/{variantTableDbId}/samples") {

            val sampleNames = call.parameters["sampleNames"] ?: ""

            val variantSetDbId =
                call.parameters["variantTableDbId"] ?: throw IllegalStateException("Must provide variantTableDbId")
            call.respond(
                SampleListResponse(
                    Metadata(),
                    SampleListResponseResult(SamplesService.taxaNames(variantSetDbId, sampleNames).toTypedArray())
                )
            )
        }

        get("/{variantTableDbId}/variants") {

            val variantTableDbId =
                call.parameters["variantTableDbId"] ?: throw IllegalStateException("Must provide variantTableDbId")

            val pageToken = call.parameters["page"]?.toInt() ?: 0 // token is reference range ID, they are stored in order
            val pageSize = call.parameters["pageSize"]?.toInt() ?: defaultVariantsPageSize

            val ranges = call.parameters["ranges"]?:""

            if(pageToken < 0) {
                // Should this check < 1 vs < 0?  Is 0 valid ??
                call.respond(" ${HttpStatusCode.NotFound}: The page number is negative.  It needs to be 0 or higher.")
            }

            val variantTableServices = VariantTablesService()

            val variantsAndPagination = variantTableServices.generateVariantsListFromGraph(variantTableDbId, pageToken, pageSize,ranges)

            val pagination = variantsAndPagination.first
            val variants = variantsAndPagination.second
            var metadata = MetadataTokenPagination(pagination = pagination)
            call.respond(VariantsListResponse(metadata, VariantsListResponseResult(variants.toTypedArray())))
        }

    }
}