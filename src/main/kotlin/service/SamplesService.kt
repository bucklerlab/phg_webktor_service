package service

import model.Sample
import org.apache.log4j.Logger

object SamplesService {

    private val myLogger = Logger.getLogger(SamplesService::class.java)

    // Cached map of all taxa. Key is genoid mapped to Sample object
    private val taxa: Map<String, Sample> by lazy {
        taxaMap()
    }

    private val taxaByName: Map<String, Sample> by lazy {
        taxa.values
            .map { Pair(it.sampleName, it) }
            .toMap()
    }

    fun allTaxaNames(): List<Sample> {
        return taxa.values.toList()
    }

    fun taxaNames(method: String, taxaToKeep: String = ""): List<Sample> {
        val graph = buildGraph(method)
        require(graph != null) { "graph for method: $method doesn't exist." }
        val keepTaxa = taxaToKeep
            .split(",")
            .map { it.trim() }
            .filter { !it.isEmpty() }
            .toSortedSet()
        println("number of samples to keep: ${keepTaxa.size} $keepTaxa")
        return graph.taxaInGraph()
            .sorted()
            .filter { keepTaxa.isEmpty() || keepTaxa.contains(it.name) }
            .map {
                taxaByName[it.name]
                    ?: throw IllegalStateException("taxon: ${it.name} from graph doesn't exist in full list from database.")
            }
            .toList()
    }

    fun taxa(id: String) = taxa[id]

    private fun taxaMap(): Map<String, Sample> {

        val sqlStatement = "SELECT distinct line_name, line_data, genoid " +
                "FROM genotypes " +
                "ORDER BY genotypes.line_name;"

        myLogger.info("allTaxaNames: query statement: $sqlStatement")

        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->

            try {

                val result = mutableMapOf<String, Sample>()

                connection.createStatement().executeQuery(sqlStatement).use { rs ->

                    val time = System.nanoTime()

                    while (rs.next()) {
                        val sampleDbId = rs.getInt("genoid").toString()
                        result.put(sampleDbId, Sample(sampleName = rs.getString("line_name"), sampleDescription = rs.getString("line_data"), sampleDbId = sampleDbId))
                    }

                    myLogger.info("allTaxaNames: number of taxa lists: ${result.size}")
                    myLogger.info("""allTaxaNames: time: ${(System.nanoTime() - time) / 1e9} secs.""")

                    return result

                }
            } catch (e: Exception) {
                myLogger.debug(e.message, e)
                e.printStackTrace()
                throw IllegalStateException("SamplesService: allTaxaNames: Problem querying the database: ${e.message}")
            }

        }

    }

}