package api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import model.NodeListResponse
import model.NodeResponse

class NodeTest : StringSpec({

    "Test /nodes/CONSENSUS/ from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/nodes/"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        val nodeResponse : NodeListResponse = mapper.readValue(result)

        val nodeList = nodeResponse.result.data

        //check number of nodes
        nodeList.size shouldBe 120
    }

    "Test /nodes/111 from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/nodes/111"
        val result = khttp.get(query).text

        val mapper = jacksonObjectMapper()

        val nodeResponse : NodeResponse = mapper.readValue(result)

        val node = nodeResponse.result
        val additionalInfo = node.additionalInfo


        node.nodeDbId shouldBe 111

        (additionalInfo["refRangeId"] as Int) shouldBe 1
        (additionalInfo["refRangeChr"] as String) shouldBe "1"
        (additionalInfo["refRangeStart"] as Int) shouldBe 1
        (additionalInfo["refRangeEnd"] as Int) shouldBe 3000
        (additionalInfo["taxaList"] as List<*>) shouldBe listOf("Ref", "RefA1")
        (additionalInfo["seqLen"] as Int) shouldBe 2992
        (additionalInfo["asmContig"] as String) shouldBe "0"
        (additionalInfo["asmStart"] as Int) shouldBe 0
        (additionalInfo["asmEnd"] as Int) shouldBe 0
        (additionalInfo["genomeFileId"] as Int) shouldBe -1

    }

})