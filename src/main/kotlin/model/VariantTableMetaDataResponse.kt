package model

import kotlinx.serialization.Serializable

@Serializable
data class VariantTableMetaDataResponse(
    val metadata: Metadata,
    val result: VariantTableMetaDataResponseResult,
    val atcontext: Context? = null
)

@Serializable
data class VariantTableMetaDataResponseResult(
    val `data`: Array<VariantTableMetaData>
)