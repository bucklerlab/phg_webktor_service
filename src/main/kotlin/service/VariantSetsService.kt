package service

import com.typesafe.config.ConfigFactory
import io.ktor.server.config.*
import io.ktor.util.*
import model.*
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import org.apache.log4j.Logger
import org.ehcache.config.builders.CacheConfigurationBuilder
import org.ehcache.config.builders.CacheManagerBuilder
import org.ehcache.config.builders.ResourcePoolsBuilder

class VariantSetsService {
    private val myLogger = Logger.getLogger(VariantSetsService::class.java)

    private val config = HoconApplicationConfig(ConfigFactory.load())

    @KtorExperimentalAPI
    val phgConfigFile = config.property("phgConfigFile").getString()

    val variantSetDbIdToHapType  = getAllDBIdsAndTypes().map { Pair(it.first,it.second) }.toMap()

    val hapIdToRefRangeMap = createHapIdToRefRangeMap()

    private val cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
            .withCache("variantSetCache",
                    CacheConfigurationBuilder.newCacheConfigurationBuilder(String::class.java,
                            VariantSet::class.java,
                            ResourcePoolsBuilder.heap(10).build()))
            .build(true)

    val variantSetCache = cacheManager.getCache("variantSetCache",String::class.java, VariantSet::class.java)

    /**
     * Function to create a hapIdToRefernceRange Map
     *
     * Will hit the database and dump the haplotypes table to get this information.
     *
     * Luckily we are only pulling down 2 integers so RAM is down.
     */
    fun createHapIdToRefRangeMap() :Map<Int,Int> {
        myLogger.info("Starting to build HapIdToRefRangeMap;")
        val query = "SELECT haplotypes_id, ref_range_id FROM haplotypes;"

        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->
            try {
                val hapIdToPaths = mutableListOf<Pair<Int,Int>>()
                connection.createStatement().executeQuery(query).use { rs ->

                    val time = System.nanoTime()

                    while (rs.next()) {
                        hapIdToPaths.add(Pair(rs.getInt(1),rs.getInt(2)))
                    }

                    myLogger.info("getHapIdToRefRangeMap: number of entries: ${hapIdToPaths.size}")
                    myLogger.info("""getHapIdToRefRangeMap: time: ${(System.nanoTime() - time) / 1e9} secs.""")

                    myLogger.info("Done building HapIdToRefRangeMap;")

                    return hapIdToPaths.toMap()
                }
            } catch (e: Exception) {
                myLogger.debug(e.message, e)
                e.printStackTrace()
                throw IllegalStateException("NodeService: getAllDBIdsAndTypes: Problem querying the database: ${e.message}")
            }
        }
    }

    /**
     * Function to create VariantSets based on the variantSetDbId
     *
     * Will handle both path methods and haplotype methods correctly.
     */
    fun generateVariantSetsFromMethodName(variantSetDbId: String) : VariantSet? {
        val (type, methodName) = parseTypeAndMethodName(variantSetDbId)
        return when(variantSetDbIdToHapType[methodName]) {
            DBLoadingUtils.MethodType.PATHS.name -> generateVariantSetsFromPathMethod(variantSetDbId)
            DBLoadingUtils.MethodType.ANCHOR_HAPLOTYPES.name, DBLoadingUtils.MethodType.ASSEMBLY_HAPLOTYPES.name -> generateVariantSetsFromHaplotypeMethod(variantSetDbId)
            else -> {
                null
            }
        }
    }

    /**
     * Function to generate variantsets from a Haplotype method.
     * This will build the correct response and cache the result for faster access later.
     */
    fun generateVariantSetsFromHaplotypeMethod(variantSetDbId: String) : VariantSet? {
        val (type, methodName) = parseTypeAndMethodName(variantSetDbId)

        if(variantSetCache.containsKey(methodName)) return variantSetCache[methodName]


        val (taxaSet, refRangeSet) = getTaxaAndRefRangeHaplotypesFromDB(methodName)

        val numCallSets = taxaSet.size

        val numVariants = refRangeSet.size

        myLogger.info("Build VariantSet objects.")
        //Make the available formats
        //VariantSetAvailableFormats (val dataFormat : DataFormatEnum, val fileFormat : FileFormatEnum, val fileURL : String)
        val availFormats = if(type == VariantTypeEnum.HAP) {
            listOf(VariantSetAvailableFormats(DataFormatEnum.JSON,FileFormatEnum.APPLICATION_JSON, "/brapi/v2/variantsets/${variantSetDbId}"),
                    VariantSetAvailableFormats(DataFormatEnum.VCF, FileFormatEnum.APPLICATION_ZIP,"")) //TODO Add in the URL where the file will sit
        }
        else {
            listOf(VariantSetAvailableFormats(DataFormatEnum.VCF, FileFormatEnum.APPLICATION_ZIP,""))
        }

        val outputVariantSet = VariantSet(mapOf(), listOf(), //Need to add in additionalInfo and analysis
                availFormats,numCallSets,
                "", "",  //Add referenceSetDBId and StudyDBId in correctly
                numVariants, variantSetDbId, variantSetDbId)

        variantSetCache.put(methodName, outputVariantSet)

        return outputVariantSet

    }

    /**
     * Method to extract out the reference range ids and the correct gameteids for all the haplotypes found with the
     * given haplotype variantSetDbId.
     *
     * This will return a 2 sets.  One for callsets(Taxon ids) and one for the variantsets(refRangeIds)
     */
    fun getTaxaAndRefRangeHaplotypesFromDB(variantSetDbId: String) : Pair<Set<Int>,Set<Int>> {
        val query = "SELECT ref_range_id, gameteid FROM haplotypes " +
                "INNER JOIN methods on haplotypes.method_id = methods.method_id " +
                "INNER JOIN gamete_haplotypes on haplotypes.gamete_grp_id = gamete_haplotypes.gamete_grp_id " +
                "WHERE methods.name='${variantSetDbId}'; "

        println(query)

        DataSource.connection.use { connection ->
            try {
                val callSetIdSet = mutableSetOf<Int>()
                val refRangeIdSet = mutableSetOf<Int>()
                var totalTime = 0L

                var queryTime = System.nanoTime()
                var totalSize = 0
                connection.createStatement().executeQuery(query).use { rs ->

                    val time = System.nanoTime()

                    while (rs.next()) {

                        refRangeIdSet.add(rs.getInt(1))
                        val timeSt = System.nanoTime()
                        callSetIdSet.add(rs.getInt(2))
                        val timeEnd = System.nanoTime()
                        totalTime += (timeEnd - timeSt)
                    }


                    println("""getHapIdToRefRangeMap: time: ${(System.nanoTime() - time) / 1e9} secs.""")
                    println("Time spent running query: ${(System.nanoTime() - queryTime) /1e9} secs.")
                    println("Time spent decoding bytes: ${totalTime/1E9} seconds.")
                    println("PathBytes Size: ${totalSize}")

                    return Pair(callSetIdSet.toHashSet(), refRangeIdSet.toHashSet() )
                }
            } catch (e: Exception) {
                myLogger.debug(e.message, e)
                e.printStackTrace()
                throw IllegalStateException("NodeService: getAllDBIdsAndTypes: Problem querying the database: ${e.message}")
            }
        }

    }

    /**
     * Method to generate VariantSets for Path based methods.
     *
     * This will also cache the results for faster access later.
     */
    fun generateVariantSetsFromPathMethod(variantSetDbId: String) : VariantSet? {
        val (type, methodName) = parseTypeAndMethodName(variantSetDbId)

        if(variantSetCache.containsKey(methodName)) return variantSetCache[methodName]


        //get all the paths for this method
        myLogger.info("Extracting Paths By Method: ${variantSetDbId}")
        val pathsForMethod = extractPathsByMethod(methodName)
        myLogger.info("Done Extracting Paths from DB for Method: ${variantSetDbId}")
        val numCallSets = pathsForMethod.first.size

        myLogger.info("Determine which reference ranges have been hit.")

        val refRangesHitSet = pathsForMethod.second
        val numVariants = refRangesHitSet.size

        myLogger.info("Build VariantSet objects.")
        //Make the available formats
        //VariantSetAvailableFormats (val dataFormat : DataFormatEnum, val fileFormat : FileFormatEnum, val fileURL : String)
        val availFormats = if(type == VariantTypeEnum.HAP) {
            listOf(VariantSetAvailableFormats(DataFormatEnum.JSON,FileFormatEnum.APPLICATION_JSON, "/brapi/v2/variantsets/${variantSetDbId}"),
                    VariantSetAvailableFormats(DataFormatEnum.VCF, FileFormatEnum.APPLICATION_ZIP,"")) //TODO Add in the URL where the file will sit
        }
        else {
            listOf(VariantSetAvailableFormats(DataFormatEnum.VCF, FileFormatEnum.APPLICATION_ZIP,""))
        }

        val outputVariantSet = VariantSet(mapOf(), listOf(), //Need to add in additionalInfo and analysis
                availFormats,numCallSets,
                "", "",  //Add referenceSetDBId and StudyDBId in correctly
                numVariants, variantSetDbId, variantSetDbId)

        variantSetCache.put(methodName, outputVariantSet)

        return outputVariantSet

    }

    /**
     * Function to extract out the correct callsets and referenceRanges from a path based method.
     *
     * This will return a 2 sets.  One for callsets(Taxon ids) and one for the variantsets(refRangeIds)
     */
    fun extractPathsByMethod(variantSetDbId: String) : Pair<Set<Int>,Set<Int>> {
        val query = "SELECT genoid, paths_data FROM paths INNER JOIN methods on paths.method_id = methods.method_id WHERE name=\'${variantSetDbId}\';"

        println(query)
        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->
            try {
                val pathIdToPath = mutableListOf<Pair<Int,List<List<Int>>>>()

                val callSetIdSet = mutableSetOf<Int>()
                val refRangeIdSet = mutableSetOf<Int>()
                var totalTime = 0L

                var queryTime = System.nanoTime()
                var totalSize = 0
                connection.createStatement().executeQuery(query).use { rs ->

                    val time = System.nanoTime()

                    while (rs.next()) {

                        callSetIdSet.add(rs.getInt(1))
                        val timeSt = System.nanoTime()
                        val pathBytes = rs.getBytes(2)
                        val pathBytesSize = pathBytes.size
                        totalSize+=pathBytesSize +4
                        val refRanges = DBLoadingUtils.decodePathsForMultipleLists(pathBytes).flatten().map { hapIdToRefRangeMap[it] }.filterNotNull().toSet()
                        val timeEnd = System.nanoTime()
                        totalTime += (timeEnd - timeSt)
                        refRangeIdSet.addAll(refRanges)

                    }

                    myLogger.info("getHapIdToRefRangeMap: number of entries: ${pathIdToPath.size}")
                    println("""getHapIdToRefRangeMap: time: ${(System.nanoTime() - time) / 1e9} secs.""")
                    println("Time spent running query: ${(System.nanoTime() - queryTime) /1e9} secs.")
                    println("Time spent decoding bytes: ${totalTime/1E9} seconds.")
                    println("PathBytes Size: ${totalSize}")

                    return Pair(callSetIdSet.toHashSet(), refRangeIdSet.toHashSet() )
                }
            } catch (e: Exception) {
                myLogger.debug(e.message, e)
                e.printStackTrace()
                throw IllegalStateException("NodeService: getAllDBIdsAndTypes: Problem querying the database: ${e.message}")
            }
        }
    }

    /**
     * Function to extractPath haplotypes using pagination
     * This will first determine the pages and then will create haplotypes for that specific page.
     */
    fun extractPathsHaplotypesByMethod(variantSetDbId: String, pageNum : Int, pageSize: Int) : Pair<TokenPagination,List<Triple<Int, Int, List<Int>>>> {
        //get list of path ids and sort
        val pathIds = getSortedPathIds(variantSetDbId)
        //batch the path ids by pageSize
        val pages = pathIds.chunked(pageSize)

        val nextPage = (pageNum+pageSize+1).toString()


        if (pageNum >= pages.size) {
            return Pair(TokenPagination(), emptyList())
        }

        val callTriples = extractPathsHaplotypesByPathIds(pages[pageNum])

        //        pagination.totalCount = pathIds.size  TODO figure out how to get the total count in a fast way.
        var pagination = TokenPagination(pageSize=callTriples.size,nextPageToken=nextPage,currentPageToken=pageNum.toString(),
                            totalPages=pages.size)

        //extract Paths for the batch
        return Pair(pagination,callTriples )
    }

    /**
     * Function to sort the path_ids for a given variantSetDbId.
     * This needs to be sorted otherwise the pages will not be consistent.
     */
    fun getSortedPathIds(variantSetDbId: String) : List<Int> {
        val query = "SELECT path_id FROM paths INNER JOIN methods on paths.method_id = methods.method_id WHERE name='${variantSetDbId}';"

        println(query)

        DataSource.connection.use { connection ->
            try {

                val outputList = mutableListOf<Int>()
                connection.createStatement().executeQuery(query).use { rs ->

                    val time = System.nanoTime()

                    while (rs.next()) {
                        outputList.add(rs.getInt(1))
                    }

                    myLogger.info("getSortedPathIds: number of entries: ${outputList.size}")
                    println("""getSortedPathIds: time: ${(System.nanoTime() - time) / 1e9} secs.""")

                    return outputList.sorted()
                }
            } catch (e: Exception) {
                myLogger.debug(e.message, e)
                e.printStackTrace()
                throw IllegalStateException("NodeService: getAllDBIdsAndTypes: Problem querying the database: ${e.message}")
            }
        }

    }

    /**
     * Function to extract the path haplotype ids by the page.  This will return the List<Triple>
     * The triples contain the callSetId, variantId and then the list of Ints representing the haplotype ids(calls)
     */
    fun extractPathsHaplotypesByPathIds(pathIds: List<Int>) : List<Triple<Int, Int, List<Int>>> {
        val query = "SELECT genoid, paths_data " +
                "FROM paths " +
                "WHERE paths.path_id in (${pathIds.joinToString(",")});"

        println(query)
        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->
            try {
                var totalTime = 0L

                var queryTime = System.nanoTime()
                var totalSize = 0

                val outputList = mutableListOf<Triple<Int, Int, List<Int>>>()
                connection.createStatement().executeQuery(query).use { rs ->

                    val time = System.nanoTime()

                    while (rs.next()) {

                        val callSetId = rs.getInt(1)
                        val timeSt = System.nanoTime()
                        val pathBytes = rs.getBytes(2)
                        val pathBytesSize = pathBytes.size
                        totalSize+=pathBytesSize +4
                        val pathHaplotypes = DBLoadingUtils.decodePathsForMultipleLists(pathBytes)

                        //.map { hapIdToRefRangeMap[it] }

                        //Walk through the pathHaplotypes
                        val pathsGrouped = mutableMapOf<Int, MutableList<Int>>()

                        for(path in pathHaplotypes) {
                            for(haplotype in path) {
                                val refRange = hapIdToRefRangeMap[haplotype] ?: -1

                                if(pathsGrouped.containsKey(refRange)) {
                                    pathsGrouped[refRange]?.add(haplotype)
                                }
                                else {
                                    pathsGrouped[refRange] = mutableListOf<Int>(haplotype)
                                }
                            }
                        }

                        val callList = pathsGrouped.map {
                            val refRangeId = it.key
                            val haplotypes = it.value

                            Triple(callSetId, refRangeId, haplotypes)
                        }

                        outputList.addAll(callList)

                        val timeEnd = System.nanoTime()
                        totalTime += (timeEnd - timeSt)


                    }

                    myLogger.info("getHapIdToRefRangeMap: number of entries: ${outputList.size}")
                    println("""getHapIdToRefRangeMap: time: ${(System.nanoTime() - time) / 1e9} secs.""")
                    println("Time spent running query: ${(System.nanoTime() - queryTime) /1e9} secs.")
                    println("Time spent decoding bytes: ${totalTime/1E9} seconds.")
                    println("PathBytes Size: ${totalSize}")

                    return outputList
                }
            } catch (e: Exception) {
                myLogger.debug(e.message, e)
                e.printStackTrace()
                throw IllegalStateException("NodeService: getAllDBIdsAndTypes: Problem querying the database: ${e.message}")
            }
        }
    }

    fun extractPathsHaplotypesByMethod(variantSetDbId: String) : List<Triple<Int, Int, List<Int>>> {
        val query = "SELECT genoid, paths_data FROM paths INNER JOIN methods on paths.method_id = methods.method_id WHERE name=\'${variantSetDbId}\';"

        println(query)
        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->
            try {
                var totalTime = 0L

                var queryTime = System.nanoTime()
                var totalSize = 0

                val outputList = mutableListOf<Triple<Int, Int, List<Int>>>()
                connection.createStatement().executeQuery(query).use { rs ->

                    val time = System.nanoTime()

                    while (rs.next()) {

                        val callSetId = rs.getInt(1)
                        val timeSt = System.nanoTime()
                        val pathBytes = rs.getBytes(2)
                        val pathBytesSize = pathBytes.size
                        totalSize+=pathBytesSize +4
                        val pathHaplotypes = DBLoadingUtils.decodePathsForMultipleLists(pathBytes)

                        //.map { hapIdToRefRangeMap[it] }

                        //Walk through the pathHaplotypes
                        val pathsGrouped = mutableMapOf<Int, MutableList<Int>>()

                        for(path in pathHaplotypes) {
                            for(haplotype in path) {
                                val refRange = hapIdToRefRangeMap[haplotype] ?: -1

                                if(pathsGrouped.containsKey(refRange)) {
                                    pathsGrouped[refRange]?.add(haplotype)
                                }
                                else {
                                    pathsGrouped[refRange] = mutableListOf<Int>(haplotype)
                                }
                            }
                        }

                        val callList = pathsGrouped.map {
                            val refRangeId = it.key
                            val haplotypes = it.value

                            Triple(callSetId, refRangeId, haplotypes)
                        }

                        outputList.addAll(callList)

                        val timeEnd = System.nanoTime()
                        totalTime += (timeEnd - timeSt)


                    }

                    myLogger.info("getHapIdToRefRangeMap: number of entries: ${outputList.size}")
                    println("""getHapIdToRefRangeMap: time: ${(System.nanoTime() - time) / 1e9} secs.""")
                    println("Time spent running query: ${(System.nanoTime() - queryTime) /1e9} secs.")
                    println("Time spent decoding bytes: ${totalTime/1E9} seconds.")
                    println("PathBytes Size: ${totalSize}")

                    return outputList
                }
            } catch (e: Exception) {
                myLogger.debug(e.message, e)
                e.printStackTrace()
                throw IllegalStateException("NodeService: getAllDBIdsAndTypes: Problem querying the database: ${e.message}")
            }
        }
    }

    /**
     * Function to paginate the haplotype based methods
     *
     * This will first get all the haplotype ids and sort them.
     * It will then build the pages and will request the results for a specific page.
     */
    fun extractHaplotypesByMethodPaginated(variantSetDbId: String, pageNum: Int, pageSize: Int) : Pair<TokenPagination,List<Triple<Int, Int, List<Int>>>> {

        val haplotypeTriples = extractHaplotypesByMethod(variantSetDbId)

        val sortedIds = haplotypeTriples.map{it.first}.toSortedSet()

        val pages = sortedIds.chunked(pageSize)



        if (pageNum >= pages.size) {
            return Pair(TokenPagination(), emptyList())
        }

        val callTriples = haplotypeTriples.filter { pages[pageNum].contains(it.first) }

//        pagination.totalCount = pathIds.size //TODO figure out a way to get total number of calls

        val pagination = TokenPagination(currentPageToken=pageNum.toString(),totalPages=pages.size,pageSize=callTriples.size)
        return Pair(pagination,callTriples )
    }

    /**
     * Function to extract the list of calls.  The triples are the callsetId, the variantId and the list of haplotype calls.
     */
    fun extractHaplotypesByMethod(variantSetDbId: String) : List<Triple<Int, Int, List<Int>>> {
        val query = "SELECT haplotypes.ref_range_id, haplotypes.haplotypes_id, gametes.genoid, haplotypes.gamete_grp_id " +
                "FROM haplotypes " +
                "INNER JOIN methods ON haplotypes.method_id = methods.method_id " +
                "INNER JOIN gamete_haplotypes ON haplotypes.gamete_grp_id = gamete_haplotypes.gamete_grp_id " +
                "INNER JOIN gametes ON gamete_haplotypes.gameteid = gametes.gameteid " +
                "WHERE methods.name='${variantSetDbId}';"

        println(query)
        // The connection is automatically close when this block finishes.
        DataSource.connection.use { connection ->
            try {
                var totalTime = 0L

                var queryTime = System.nanoTime()
                var totalSize = 0

                connection.createStatement().executeQuery(query).use { rs ->

                    val time = System.nanoTime()

                    val callMap = mutableMapOf<Pair<Int,Int>, MutableMap<Int, Int>>()

                    while (rs.next()) {

                        val refRangeId = rs.getInt(1)
                        val haplotypeId = rs.getInt(2)
                        val genoId = rs.getInt(3)
                        val gamete_grp_id = rs.getInt(4)

                        val key = Pair(refRangeId, genoId)

                        //This is a bit tricky, we need to store a mapping of haplotype ids to handle the multiple gametes.
                        //This also will work with consensus as we use refRangeId and genoId as the keys.
                        if(callMap.containsKey(key)) {
                            callMap[key]?.put(gamete_grp_id, haplotypeId)
                        }
                        else {
                            callMap[key] = mutableMapOf(gamete_grp_id to haplotypeId)
                        }

                    }

                    myLogger.info("getHapIdToRefRangeMap: number of entries: ${callMap.size}")
                    println("""getHapIdToRefRangeMap: time: ${(System.nanoTime() - time) / 1e9} secs.""")
                    println("Time spent running query: ${(System.nanoTime() - queryTime) /1e9} secs.")
                    println("Time spent decoding bytes: ${totalTime/1E9} seconds.")
                    println("PathBytes Size: ${totalSize}")

                    //We need to then loop through each refRangeXgenoId and convert the gamete map into a list correctly.
                    return callMap.keys.map { refRangeAndCallSet ->
                        val gametesAndHaplotypes = callMap[refRangeAndCallSet]!!

                        val sortedHaplotypeIds = gametesAndHaplotypes.keys
                                                                .sorted()
                                                                .map { gametesAndHaplotypes[it]!! }

                        Triple(refRangeAndCallSet.second, refRangeAndCallSet.first, sortedHaplotypeIds)
                    }
                }
            } catch (e: Exception) {
                myLogger.debug(e.message, e)
                e.printStackTrace()
                throw IllegalStateException("NodeService: getAllDBIdsAndTypes: Problem querying the database: ${e.message}")
            }
        }
    }


    /**
     * Simple method to generate all the VariantSets in the database.
     * Basically will walk through each variantSetDbId and will build the Variantset of each.
     */
    fun generateVariantSets(): List<VariantSet> {
        //Need to get every haplotype and path method in the db.  Then call generateVariantSetFromGraph(variantSetDBId)
        return variantSetDbIdToHapType.keys
                .mapNotNull { generateVariantSetsFromMethodName(it) } //Can do this safely because these Ids were taken directly from the DB
    }

    fun generateVariantSetsFromGraph(): List<VariantSet> {
        //Need to get every haplotype and path method in the db.  Then call generateVariantSetFromGraph(variantSetDBId)
        return variantSetDbIdToHapType.keys
                .mapNotNull { generateVariantSetFromGraph(it) } //Can do this safely because these Ids were taken directly from the DB
    }

    fun generateVariantSetFromGraph(variantSetDbId : String) : VariantSet? {
        val (type, methodName) = parseTypeAndMethodName(variantSetDbId)

        myLogger.info("Finding Graph for : ${methodName}")
        val graph = buildGraph(methodName) ?: return null

        //Make the available formats
        //VariantSetAvailableFormats (val dataFormat : DataFormatEnum, val fileFormat : FileFormatEnum, val fileURL : String)
        val availFormats = if(type == VariantTypeEnum.HAP) {
            listOf(VariantSetAvailableFormats(DataFormatEnum.JSON,FileFormatEnum.APPLICATION_JSON, "/brapi/v2/variantsets/${variantSetDbId}"),
                    VariantSetAvailableFormats(DataFormatEnum.VCF, FileFormatEnum.APPLICATION_ZIP,"")) //TODO Add in the URL where the file will sit
        }
        else {
            listOf(VariantSetAvailableFormats(DataFormatEnum.VCF, FileFormatEnum.APPLICATION_ZIP,""))
        }


        return VariantSet(mapOf(), listOf(), //Need to add in additionalInfo and analysis
                availFormats,graph.totalNumberTaxa(),
                "", "",  //Add referenceSetDBId and StudyDBId in correctly
                graph.referenceRanges().size, variantSetDbId, variantSetDbId)

    }

    fun generateVariantsFromVariantSetId(variantSetDbId: String) : List<Variant> {
        val (type, methodName) = parseTypeAndMethodName(variantSetDbId)

        val graph = buildGraph(methodName) ?: return listOf()

        val variantService = VariantsService()

        return when(type) {
            VariantTypeEnum.HAP -> variantService.buildRefRangeVariantsFromGraph(graph, variantSetDbId)
            else -> {
                listOf()
            }
        }
    }

    /**
     * Function to get the calls from a given variantSetDbId.
     *
     * First this will check to see if the method represents a path or haplotype method and will extract the correct calls.
     *
     * Right now we only support Haplotype based variants.
     *
     * This will also determine the pagination to be returned as well based on the number of paths or number of taxon for this method.
     */
    fun generateCallsFromMethodName(variantSetDbId: String, pageNum : Int, pageSize : Int) : Pair<TokenPagination,List<Call>> {
        val (type, methodName) = parseTypeAndMethodName(variantSetDbId)

        val callSetIdToHaplotypeList = when(variantSetDbIdToHapType[methodName]) {
            DBLoadingUtils.MethodType.PATHS.name -> extractPathsHaplotypesByMethod(methodName, pageNum, pageSize)
            DBLoadingUtils.MethodType.ANCHOR_HAPLOTYPES.name, DBLoadingUtils.MethodType.ASSEMBLY_HAPLOTYPES.name -> extractHaplotypesByMethodPaginated(methodName, pageNum, pageSize)
            else -> {

                Pair(TokenPagination(),listOf<Triple<Int, Int, List<Int>>>())
            }
        }

        if(callSetIdToHaplotypeList.second.isEmpty()) {
            return Pair(callSetIdToHaplotypeList.first,listOf())
        }

        val callSetsService = CallSetsService()
        val callSetIds = callSetIdToHaplotypeList.second.map { it.first }.toSet()
        val callsService = CallsService()

        return when(type) {
            VariantTypeEnum.HAP -> Pair(callSetIdToHaplotypeList.first,callsService.buildCallsFromCallList(methodName,callSetIdToHaplotypeList.second, callSetIdToTaxonMap = callSetsService.buildGenoIdToNameMap(callSetIds)))
            else -> {
                Pair(callSetIdToHaplotypeList.first,listOf())
            }
        }
    }

    /**
     * Function to get the callsets from a given variantSetDbId.
     *
     * First it will check to see if its a path or haplotype method and will extract out the taxaIds from the
     * respective database tables.
     *
     * This will return a List<CallSet> to be sent to the client.
     *
     * Right now we only support Haplotype based variants.
     */
    fun generateCallsetsFromMethodName(variantSetDbId: String) : List<CallSet> {
        val (type, methodName) = parseTypeAndMethodName(variantSetDbId)

        val (taxaIds, refRangeIds) = when(variantSetDbIdToHapType[methodName]) {
            DBLoadingUtils.MethodType.PATHS.name -> extractPathsByMethod(methodName)
            DBLoadingUtils.MethodType.ANCHOR_HAPLOTYPES.name, DBLoadingUtils.MethodType.ASSEMBLY_HAPLOTYPES.name -> getTaxaAndRefRangeHaplotypesFromDB(methodName)
            else -> {
                Pair(setOf(), setOf())
            }
        }

        if(taxaIds.isEmpty()) {
            return listOf()
        }

        val callSetsService = CallSetsService()

        return when(type) {
            VariantTypeEnum.HAP -> callSetsService.buildCallSetsFromIds(methodName, taxaIds)
            else -> {
                listOf()
            }
        }

    }

    /**
     * Function to get the List<Variants> from the given variantSetDbId.
     *
     * It will first check to see if its a path or haplotype method.
     *
     * If its a path method, it will call extractPathsByMethod(methodName)
     * but if its a haplotype, it will call getTaxaAndRefRangeHaplotypesFromDB(methodName) as they need to call against different tables.
     *
     *
     * Right now we only support Haplotype based variants.
     */
    fun generateVariantsFromMethodName(variantSetDbId: String) : List<Variant> {
        val (type, methodName) = parseTypeAndMethodName(variantSetDbId)

        val (taxaIds, refRangeIds) = when(variantSetDbIdToHapType[methodName]) {
            DBLoadingUtils.MethodType.PATHS.name -> extractPathsByMethod(methodName)
            DBLoadingUtils.MethodType.ANCHOR_HAPLOTYPES.name, DBLoadingUtils.MethodType.ASSEMBLY_HAPLOTYPES.name -> getTaxaAndRefRangeHaplotypesFromDB(methodName)
            else -> {
                Pair(setOf(), setOf())
            }
        }

        if(refRangeIds.isEmpty()) {
            return listOf()
        }

        val variantService = VariantsService()

        return when(type) {
            VariantTypeEnum.HAP -> variantService.buildRefRangeVariantsFromIds(methodName, refRangeIds)
            else -> {
                listOf()
            }
        }
    }


    fun generateCallSetsFromVariantSetId(variantSetDbId: String) : List<CallSet> {
        //build the graph without sequence
        val (type, methodName) = parseTypeAndMethodName(variantSetDbId)

        val graph = buildGraph(methodName) ?: return listOf()

        val callService = CallSetsService()

        return when(type) {
            VariantTypeEnum.HAP -> callService.generateCallSetsFromGraph(graph)
            else -> {
                listOf()
            }
        }
    }

    fun generateCallsFromVariantSetId(variantSetDbId: String) : List<Call> {
        //build the graph without sequence
        val (type, methodName) = parseTypeAndMethodName(variantSetDbId)

        val graph = buildGraph(methodName) ?: return listOf()

        val callService = CallsService()

        return when(type) {
            VariantTypeEnum.HAP -> callService.buildCallListFromGraph(graph)
            else -> {
                listOf()
            }
        }
    }



}