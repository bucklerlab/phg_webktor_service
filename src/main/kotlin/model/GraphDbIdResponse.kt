package model

data class GraphDbIdResponse( val metadata: Metadata,
                                  val result: GraphDbIdResponseResult,
                                  var Atcontext: Context? = null)

data class GraphDbIdResponseResult(val data: Array<GraphDbIdModel>)

data class GraphDbIdModel(val name : String, val type : String)