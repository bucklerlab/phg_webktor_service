package api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import model.ReferenceSetsListResponse
import model.ReferenceSetsSingleResponse
import model.ReferenceSingleResponse
import model.ReferencesListResponse

/**
 * Class to test brAPI Reference related endpoings: ReferenceSets
 * and References
 *
 * @author lcj34 March 2021
 */
class ReferencesAndReferenceSetsTests: StringSpec({

    "Test ReferenceSets from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/referencesets"
        val result = khttp.get(query).text
        println("\n Result from test: ${result}")

        val mapper = jacksonObjectMapper()

        val resultAsList: ReferenceSetsListResponse = mapper.readValue(result)
        println("\nresultAsList size: \${resultAsList.result.data.size")
        resultAsList.result.data.size shouldBe 1
    }
    "Test ReferenceSets By Id from smallSeqDB" {
        // NOTE - the ref name will change based on the db to which this test connects
        // Ref_Assembly is the value when connecting to phgSmallSeq.db
        // B73Ref is the value when connecting to our NAM postgres db
        val query = "http://localhost:8080/brapi/v2/referencesets/Ref_Assembly"
        val result = khttp.get(query).text
        println("\n Result from test: $result")

        val mapper = jacksonObjectMapper()

        val resultValue: ReferenceSetsSingleResponse = mapper.readValue(result)
        println("\nresultValue : ${resultValue.result}")
        resultValue.result.referenceSetDbId shouldBe "1"
    }
    "Test References from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/references"
        val result = khttp.get(query).text
        println("\n Result from test: ${result}")

        val mapper = jacksonObjectMapper()

        val resultAsList: ReferencesListResponse = mapper.readValue(result)
        println("\nresultAsList size: \${resultAsList.result.data.size")
        resultAsList.result.data.size shouldBe 1 // because this is smallseq - only 1 chrom!
        resultAsList.result.data[0].referenceName shouldBe "1"
    }
    "Test References By Id from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/references/1"
        val result = khttp.get(query).text
        println("\n Result from test: $result")

        val mapper = jacksonObjectMapper()

        val resultValue: ReferenceSingleResponse = mapper.readValue(result)
        println("\nresultValue : ${resultValue.result}")
        resultValue.result.referenceSetDbId shouldBe "1"
    }
})