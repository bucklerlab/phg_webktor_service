package api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.kotest.core.spec.style.StringSpec
import model.SampleListResponse
import model.VariantTableResponse

class VariantTableSamplesTest : StringSpec({

    "Test /variantTables/HAP_PATH_METHOD/samples from smallSeqDB" {

        // TO run this test, the application.conf file should have data to point to
        // the default phgsmallseq_test db on cbsudc01.  The phg_config_file should
        // also be a config file with data to connect to phsmallseq_test
        // Using page size of 1 for small seq to verify it loops through all pages.
        //val samplesQuery = "http://localhost:8080/brapi/v2/variantTables/mummer4/samples"
        val samplesQuery = "http://localhost:8080/brapi/v2/variantTables/PATH_METHOD/samples"
        val samplesResult = khttp.get(samplesQuery).text
        println("\n Result from test: $samplesResult")

        val mapper = jacksonObjectMapper()

        val samplesListResponse: SampleListResponse = mapper.readValue(samplesResult)
        val samplesListResponseResult = samplesListResponse.result

        val samplesData = samplesListResponse.result.data
        println("num of samples: ${samplesData.size}")
        check(samplesData.size == 28) { "Num of samples should be 28" }

    }
    "Test /variantTables/HAP_PATH_METHOD/samples?sampleNames from smallSeqDB" {

        // TO run this test, the application.conf file should have data to point to
        // the default phgsmallseq_test db on cbsudc01.  The phg_config_file should
        // also be a config file with data to connect to phsmallseq_test
        // Using page size of 1 for small seq to verify it loops through all pages.
        val sampleNames = listOf("LineA1_gbs", "LineA1_wgs", "LineB1_gbs", "LineB_wgs", "RecLineA1LineA1gco2_wgs")
        val samplesQuery =
            "http://localhost:8080/brapi/v2/variantTables/PATH_METHOD/samples?sampleNames=${sampleNames.joinToString(",")}"
        val samplesResult = khttp.get(samplesQuery).text
        println("\n Result from test: $samplesResult")

        val mapper = jacksonObjectMapper()

        val samplesListResponse: SampleListResponse = mapper.readValue(samplesResult)
        val samplesListResponseResult = samplesListResponse.result

        val samplesData = samplesListResponse.result.data
        println("num of samples: ${samplesData.size}")
        check(samplesData.size == sampleNames.size) { "Num of samples should be sampleNames.size" }

        samplesData.forEach {
            check(sampleNames.contains(it.sampleName)) { "Results contains ${it.sampleName} but wasn't in the original list" }
        }

    }

    "Test /variantTables/HAP_PATH_METHOD/table?sampleNames from smallSeqDB" {

        // TO run this test, the application.conf file should have data to point to
        // the default phgsmallseq_test db on cbsudc01.  The phg_config_file should
        // also be a config file with data to connect to phsmallseq_test
        // Using page size of 1 for small seq to verify it loops through all pages.
        val sampleNames = listOf("LineA1_gbs", "LineA1_wgs", "LineB1_gbs", "LineB_wgs", "RecLineA1LineA1gco2_wgs")
        val variantTableQuery =
            "http://localhost:8080/brapi/v2/variantTables/PATH_METHOD/table?sampleNames=${sampleNames.joinToString(",")}"
        val variantTableResult = khttp.get(variantTableQuery).text
        println("\n Result from test: $variantTableResult")

        val mapper = jacksonObjectMapper()

        val variantTableResponse: VariantTableResponse = mapper.readValue(variantTableResult)
        val samplesListResponseResult = variantTableResponse.result

        val genotypesData = variantTableResponse.result.genotypes
        println("num of ranges: ${genotypesData.size}")
        println("num of taxa: ${genotypesData[0].size}")
        check(genotypesData[0].size == sampleNames.size) { "Num of samples should be sampleNames.size" }

    }
})