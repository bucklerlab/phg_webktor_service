package model

import kotlinx.serialization.Serializable
import java.util.*

@Serializable
class Edge( val edgeDbId : String, val leftNodeDbId : Int, val rightNodeDbId : Int, val weight : Double) {
    override fun equals(o: Any?): Boolean {
        if (this === o) {
            return true
        }
        if (o == null || o !is Edge) {
            return false
        }
        val edge: Edge = o as Edge
        return edgeDbId == edge.edgeDbId &&
                leftNodeDbId == edge.leftNodeDbId &&
                rightNodeDbId == edge.rightNodeDbId &&
                weight == edge.weight
    }

    override fun hashCode(): Int {
        return Objects.hash(edgeDbId, leftNodeDbId, rightNodeDbId, weight)
    }

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("class Edge {\n")
        sb.append("    edgeDbId: ").append(toIndentedString(edgeDbId)).append("\n")
        sb.append("    leftNodeDbId: ").append(toIndentedString(leftNodeDbId)).append("\n")
        sb.append("    rightNodeDbId: ").append(toIndentedString(rightNodeDbId)).append("\n") //This one might need work
        sb.append("    weight: ").append(toIndentedString(weight)).append("\n")
        sb.append("}")
        return sb.toString()
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private fun toIndentedString(o: Any?): String? {
        return o?.toString()?.replace("\n", "\n    ") ?: "null"
    }
}