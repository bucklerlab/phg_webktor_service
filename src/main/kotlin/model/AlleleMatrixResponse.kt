package model

import kotlinx.serialization.Serializable

@Serializable
data class AlleleMatrixResponse(
    val metadata: MetadataMatrixPagination?,
    val result: AlleleMatrix,
    val atcontext: Context? = null
)