package api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import htsjdk.variant.variantcontext.VariantContext
import htsjdk.variant.vcf.VCFFileReader
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import model.*
import java.io.File

class VariantSetsTest : StringSpec({
    val truthVCFIterator = VCFFileReader(File("/Users/lcj34/notes_files/phg_2018/phg_web_service/smallSeq_pathMethodHap.vcf"), false).iterator()
    //val truthVCFIterator = VCFFileReader(File("/Users/zrm22/Desktop/smallSeq_pathMethodHap.vcf"), false).iterator()

    val variants = mutableListOf<VariantContext>()

    while(truthVCFIterator.hasNext()) {
        variants.add(truthVCFIterator.next())
    }

    val taxaSet = variants.flatMap{ it.sampleNames }.toSet()


    "Test /variantsets/HAP_PATH_METHOD from smallSeqDB" {
        val query = "http://localhost:8080/brapi/v2/variantsets/HAP_PATH_METHOD/callsets"
        val result = khttp.get(query).text
        println("\n Result from test: $result") // lcj test

        val mapper = jacksonObjectMapper()

        val variantSet: CallSetsListResponse = mapper.readValue(result)

        variantSet.result.data.size shouldBe 28
    }
    "Test /variantsets/HAP_PATH_METHOD against VCF file" {

        val variantSetQuery = "http://localhost:8080/brapi/v2/variantsets/HAP_PATH_METHOD"
        val variantSetResult = khttp.get(variantSetQuery).text


        val mapper = jacksonObjectMapper()

        val variantSet: VariantSetResponse = mapper.readValue(variantSetResult)

        variantSet.result.variantCount shouldBe variants.size
        variantSet.result.callSetCount shouldBe taxaSet.size

    }
    "Test /variantset/HAP_PATH_METHOD/callsets against the VCF file" {
        val callSetQuery = "http://localhost:8080/brapi/v2/variantsets/HAP_PATH_METHOD/callsets"
        val callSetResult = khttp.get(callSetQuery).text

        val mapper = jacksonObjectMapper()

        val callSetList: CallSetsListResponse = mapper.readValue(callSetResult)

        val dbCallSetTaxonSet = mutableSetOf<String>()

        for(callSetList in callSetList.result.data) {
            val currentTaxa = callSetList.callSetName
            taxaSet.contains(currentTaxa) shouldBe true

            dbCallSetTaxonSet.add(currentTaxa!!)
        }

        for(taxon in taxaSet) {
            dbCallSetTaxonSet.contains(taxon) shouldBe true
        }

    }
    "Test /variantset/HAP_PATH_METHOD/variants against the VCF file" {
        val variantsQuery = "http://localhost:8080/brapi/v2/variantsets/HAP_PATH_METHOD/variants"
        val variantsResult = khttp.get(variantsQuery).text

        val mapper = jacksonObjectMapper()

        val variantsList: VariantsListResponse = mapper.readValue(variantsResult)

        val chrPlusStart = variantsList.result.data.map { "${it.referenceName}:${it.start}_${it.end}" }.toSet()
        val variantsCoords = variants.map { "${it.contig}:${it.start}_${it.end}" }.toSet()

        chrPlusStart.filter { !variantsCoords.contains(it) }.size shouldBe 0
        variantsCoords.filter { !chrPlusStart.contains(it) }.size shouldBe 0

    }

    "Test /variantset/HAP_PATH_METHOD/calls against the VCF file" {

        val callSetQuery = "http://localhost:8080/brapi/v2/variantsets/HAP_PATH_METHOD/callsets"
        val callSetResult = khttp.get(callSetQuery).text

        val callSetMapper = jacksonObjectMapper()

        val callSetList: CallSetsListResponse = callSetMapper.readValue(callSetResult)

        val callsetNameToIdMap = callSetList.result.data.map { Pair(it.callSetDbId, it.callSetName) }.toMap()

        val variantsQuery = "http://localhost:8080/brapi/v2/variantsets/HAP_PATH_METHOD/variants"
        val variantsResult = khttp.get(variantsQuery).text

        val variantMapper = jacksonObjectMapper()

        val variantsList: VariantsListResponse = variantMapper.readValue(variantsResult)

        val variantIdToNameMap = variantsList.result.data.map {
            val id = it.variantDbId
            Pair(id,"${it.referenceName}:${it.start}_${it.end}" )
        }.toMap()

        val variantTable = variants.flatMap {
            val refRangeId = "${it.contig}:${it.start}_${it.end}"
            val samples = it.sampleNames
            samples.map { sample ->
                Pair(Pair(refRangeId,sample),it.getGenotype(sample).alleles
                                                .map { allele -> allele.displayString
                                                                        .replace("<","") //Need to remove the <> from the symbolic allele.
                                                                        .replace(">","") })
            }
        }.toMap()

        val callsQuery = "http://localhost:8080/brapi/v2/variantsets/HAP_PATH_METHOD/calls"
        val callsResult = khttp.get(callsQuery).text

        val mapper = jacksonObjectMapper()

        val callsList: CallsListResponse = mapper.readValue(callsResult)

        //Check to make sure each call matches correctly.
        callsList.result.data.map {
            val variantDBId = it.variantDbId
            val callsetDBId = it.callSetDbId
            val call = it.genotype.values?.map { allele -> allele.toString() } ?: ""

            variantTable[Pair(variantIdToNameMap[variantDBId], callsetNameToIdMap[callsetDBId])] shouldBe call
        }
    }

})
