package api

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import model.Metadata
import model.AlleleMatrixResponse
import model.VariantTableMetaDataResponse
import model.VariantTableMetaDataResponseResult
import service.AlleleMatrixService
import service.variantTablesMetaData

/**
 * This Route function denotes all the GET AlleleMatrix endpoints.
 * In the PHG we handle would like to handle both SNP and HAP versions of the AlleleMatrix, but for now we only return
 * haplotype calls for both graphs or paths.  The user can specify this by providing the correct variantSetDbId
 *
 * The variantSetDbId in this case represents a method name in the PHG.  We can handle both Haplotype and Path methods.
 * If the user wants to bring out the SNPs, they will need to provide SNP_methodName as the variantSetDbId.
 * If they would like haplotypes they will need to provide either HAP_methodName or just methodName.
 *
 * Each of these will first build a graph based on the provided method and will then extract out the needed information
 * depending on the endpoint.  We also implement caching so we speed up the graph creation.
 */
fun Route.alleleMatrix() {

    get("/allelematrix") {

        val variantSetDbId = call.parameters["variantSetDbId"]

        if (variantSetDbId == null || variantSetDbId.isEmpty()) {
            call.respond(
                VariantTableMetaDataResponse(
                    Metadata(),
                    VariantTableMetaDataResponseResult(variantTablesMetaData())
                )
            )
        }

        val variantPage = call.parameters["variantPage"]?.toInt() ?: 0
        val variantPageSize = call.parameters["variantPageSize"]?.toInt() ?: 10
        val callSetPage = call.parameters["callSetPage"]?.toInt() ?: 0
        val callSetPageSize = call.parameters["callSetPageSize"]?.toInt() ?: 10


        val ranges = call.parameters["ranges"] ?: ""
        val sampleNames = call.parameters["sampleNames"] ?: ""

        val matrix = AlleleMatrixService.alleleMatrix(variantSetDbId!!, variantPageSize,variantPage, callSetPageSize, callSetPage, ranges, sampleNames)
//        val matrix = AlleleMatrixService.getBigDummyMatrix()

        if (matrix == null) {
            call.respond(" ${HttpStatusCode.NotFound}: The requested VariantMatrix object ${variantSetDbId} was not found in the database")
        } else {
            val currentResponse = AlleleMatrixResponse(
                null, // MetadataMatrixPagination(pagination = pagination),
                matrix
            )
            call.respond(currentResponse)
        }

    }

    route("/search/allelematrix") {

        post("/") {
            call.respond(" ${HttpStatusCode.NotImplemented}: The endpoint /brapi/v2/search/allelematrix is not implemented yet.")
        }

        get("/{searchResultsDbId}/") {
            call.respond(" ${HttpStatusCode.NotImplemented}: The endpoint /brapi/v2/search/allelematrix/{searchResultsDbId} is not implemented yet.")
        }

    }

}
