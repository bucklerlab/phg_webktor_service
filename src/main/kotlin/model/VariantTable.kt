package model

import kotlinx.serialization.Serializable

@Serializable
data class VariantTable(
    // List<List<"Int/Int">> 2d Array Rows are variants(RefRanges) columns are Taxa
    var genotypes: List<List<String>>,
    var phased: Boolean = false,
    // Additional arbitrary info
    var additionalInfo: Map<String, String>? = null
)
