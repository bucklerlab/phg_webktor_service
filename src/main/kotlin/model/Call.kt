package model

import kotlinx.serialization.Serializable


/**
 * A `Call` represents the determination of genotype with respect to a particular `Variant`.  It may include associated information such as quality and phasing. For example, a call might assign a probability of 0.32 to the occurrence of a SNP named RS_1234 in a call set with the name NA_12345.
 * @param additionalInfo Additional arbitrary info
 * @param callSetDbId The ID of the call set this variant call belongs to.  If this field is not present, the ordering of the call sets from a `SearchCallSetsRequest` over this `VariantSet` is guaranteed to match the ordering of the calls on this `Variant`. The number of results will also be the same.
 * @param callSetName The name of the call set this variant call belongs to. If this field is not present, the ordering of the call sets from a `SearchCallSetsRequest` over this `VariantSet` is guaranteed to match the ordering of the calls on this `Variant`. The number of results will also be the same.
 * @param genotype
 * @param genotypeLikelihood The genotype likelihood for this variant call. Each array entry represents how likely a specific genotype is for this call as log10(P(data | genotype)), analogous to the GL tag in the VCF spec. The value ordering is defined by the GL tag in the VCF spec.
 * @param phaseSet If this field is populated, this variant call's genotype ordering implies the phase of the bases and is consistent with any other variant calls on the same contig which have the same phase set string.
 * @param variantDbId The ID of the variant this call belongs to.
 * @param variantName The name of the variant this call belongs to.
 */
@Serializable
data class Call( val additionalInfo : Map<String,String> = mapOf(), val callSetDbId : String, val callSetName : String,
                 val genotype : ListValue, val genotypeLikelihood : List<Double> = listOf(), val phaseSet : String? = null,
                 val variantDbId : String, val variantName : String)