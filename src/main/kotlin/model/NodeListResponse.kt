package model

data class NodeListResponse(
        val metadata: Metadata,
        val result: NodeListResponseResult,
        var Atcontext: Context? = null
)

data class NodeListResponseResult(val data: Array<Node>)